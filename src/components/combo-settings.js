import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Chip from '@material-ui/core/Chip';
import {styles, CustomTableCell, BootstrapInput} from './../styles/combo-settings';

/* 
const CustomTableCell = withStyles(theme => ({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    }
  }))(TableCell);
  
  const styles = theme => ({
    root: {
      width: '100%',
      marginTop: theme.spacing.unit * 3,
      overflowX: 'auto',
    },
    button: {
      margin: theme.spacing.unit,
    },
    table: {
      maxWidth: 700,
    },
    row: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.background.default,
      },
    },
    header : {
      fontSize:12,
      color : '#333',
      fontWeight : 600,
      height: 'auto',
    },
    header_padding:
    {
      padding: '10px 18px 10px 16px',
      border : 'none',
      background : '#FFF',
      color : '#000'
    },
    total_cell : {
      padding: '4px 18px 4px 16px',
      color : '#5682FC'
    },
    leftBox :
    {
       float: "left",
       width : "50%",
       padding: "2.5rem 5px 1rem"
    },
    add_exception_label :{
      color : "#5682FC"
    },
    margin  :{
      margin : '0 10px'
    },
    table_row : {
      height : 'auto'
    },
    cancelButton :{
       margin : theme.spacing.unit,
       color : '#000'
    },
    button_footer : {
       textAlign : 'center',
       padding : '10px 0',
       width : '100%',
       float : 'left'
    },
    saveButton : {
      background : '#F6DD97',
      color : '#000000'
    },
    dropDownPadding : {
        padding: '5px 18px'
    },
    chips: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    chip: {
      margin: theme.spacing.unit / 4,
    },
    noLabel: {
      marginTop: theme.spacing.unit * 3,
    },
  });
  
  const BootstrapInput = withStyles(theme => ({
    root: {
      'label + &': {
        marginTop: theme.spacing.unit * 3,
      },
    },
    input: {
      borderRadius: 4,
      position: 'relative',
      backgroundColor: theme.palette.background.paper,
      border: '1px solid #ced4da',
      fontSize: 14,
      width: 'auto',
      minWidth : '120px',
      //padding: '10px 26px 10px 12px',
      padding: '10px 18px',
      transition: theme.transitions.create(['border-color', 'box-shadow']),
      textAlign : 'center',
      // Use the system font instead of the default Roboto font.
      fontFamily: [
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
      '&:focus': {
        borderRadius: 4,
        borderColor: '#80bdff',
        boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
      },
    },
  }))(InputBase); */


  function getStyles(name, that) {
    return {
      fontWeight:
        that.state.name.indexOf(name) === -1
          ? that.props.theme.typography.fontWeightRegular
          : that.props.theme.typography.fontWeightMedium,
    };
  }
  
  
  
  class ComboSettings extends React.Component{

    state = {
      name: [],
    };
  
    handleChange = event => {
      this.setState({ name: event.target.value });
    };
  
    handleChangeMultiple = event => {
      const { options } = event.target;
      const value = [];
      for (let i = 0, l = options.length; i < l; i += 1) {
        if (options[i].selected) {
          value.push(options[i].value);
        }
      }
      this.setState({
        name: value,
      });
    };

    render(){
      
      const {classes} = this.props;
      const ITEM_HEIGHT = 48;
      const ITEM_PADDING_TOP = 8;
      const MenuProps = {
        PaperProps: {
          style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
          },
        },
      };

    const buttonFooter = <div className={classes.button_footer}>
                            <Button variant="outlined" className={classes.cancelButton}>
                                CANCEL
                            </Button>
                            &nbsp; &nbsp;
                            <Button variant="contained" color="primary"  className={classes.saveButton}>
                                SAVE
                            </Button>
                         </div>

    {/*
    const dropDown1 =  <FormControl >  
                            <NativeSelect value=""  input={<BootstrapInput name="none1s" id="non1s-customized-native-simple"  />}>
                                <option value="">None / 1S</option>
                            </NativeSelect>
    </FormControl>*/}

    const names = [
                    'None',
                    '1S',
                    'All'
                  ];

    const dropDown1 = <FormControl className={classes.formControl} style={{width : '150px'}}>  <InputLabel htmlFor="select-multiple-chip">Select</InputLabel>
                        <Select multiple value={this.state.name} onChange={this.handleChange} input={<Input id="select-multiple-chip" />}
                        renderValue={selected => (
                            <div className={classes.chips}>
                            {selected.map(value => (
                                <Chip key={value} label={value} className={classes.chip} />
                            ))}
                            </div>
                        )}
                        MenuProps={MenuProps}
                        >
                        {names.map(name => (
                            <MenuItem key={name} value={name} style={getStyles(name, this)}>
                            {name}
                            </MenuItem>
                        ))}
                        </Select>
                     </FormControl>

    const dropDown2 =  <FormControl >  
                            <NativeSelect value=""  input={<BootstrapInput name="none1s" id="non1s-customized-native-simple" />}>
                                <option value="">None / 1S / 2D</option>
                            </NativeSelect>
                        </FormControl>
    
    const dropDown3 =  <FormControl >  
                            <NativeSelect value="" input={<BootstrapInput name="none1s" id="non1s-customized-native-simple" />}>
                                <option value="">None / 1S / 2D</option>
                            </NativeSelect>
                        </FormControl>

      return(
          <div>
              <Table className={classes.table}> 
                  <TableHead className={classes.header}>
                    <CustomTableCell align="left" className={classes.header_padding}>&nbsp;</CustomTableCell>
                      <CustomTableCell align="left" className={classes.header_padding}>Main Course</CustomTableCell>
                      <CustomTableCell align="left" className={classes.header_padding}>Starter</CustomTableCell>
                      <CustomTableCell align="left" className={classes.header_padding}>Drinks</CustomTableCell>
                  </TableHead>

                  <TableRow className={classes.header}>
                      <CustomTableCell align="left" className={classes.header_padding}>
                        Person 1
                      </CustomTableCell>
                      <CustomTableCell align="left" className={classes.header_padding}>
                          {dropDown1} 
                      </CustomTableCell>
                      <CustomTableCell align="left" className={classes.header_padding}>
                        {dropDown1} 
                      </CustomTableCell>
                      <CustomTableCell align="left" className={classes.header_padding}>
                        {dropDown1}  
                    </CustomTableCell>
                      
                  </TableRow>

                  <TableRow className={classes.header}>
                      <CustomTableCell align="left" className={classes.header_padding}>
                        Person 1
                      </CustomTableCell>
                      <CustomTableCell align="left" className={classes.header_padding}>
                          {dropDown1}
                      </CustomTableCell>
                      <CustomTableCell align="left" className={classes.header_padding}>
                          {dropDown1}
                      </CustomTableCell>
                      <CustomTableCell align="left" className={classes.header_padding}>
                          {dropDown1}
                    </CustomTableCell>
                      
                  </TableRow>
                  <TableRow className={classes.header}>
                      <CustomTableCell align="left" className={classes.header_padding}>
                        Person 1
                      </CustomTableCell>
                      <CustomTableCell align="left" className={classes.header_padding}>
                          {dropDown1}
                      </CustomTableCell>
                      <CustomTableCell align="left" className={classes.header_padding}>
                          {dropDown1}
                      </CustomTableCell>
                      <CustomTableCell align="left" className={classes.header_padding}>
                          {dropDown1}
                      </CustomTableCell>
                  </TableRow>
              </Table>
              <div> &nbsp; </div>
              {/*buttonFooter*/}
          </div>
              
      )
    }

  }  


ComboSettings.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(ComboSettings);