import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';
import InputBase from '@material-ui/core/InputBase';
import AccountInfo from './account-info';
import ServingZipCodes1 from './kitchen-serving-zipcode';
import OperationHours from './operation-hour';
import styles from './../styles/kitchen-details1';


let id = 0;
function createData(name, city, state, status, zipcodes, orderscap, drivers, subscribers, products, lastwkorder) {
  id += 1;
  return { name, city, state, status, zipcodes, orderscap, drivers, subscribers, products, lastwkorder };
}




const BootstrapInput = withStyles(theme => ({
    root: {
      'label + &': {
        marginTop: theme.spacing.unit * 3,
      },
    },
    input: {
      borderRadius: 4,
      position: 'relative',
      backgroundColor: theme.palette.background.paper,
      border: '1px solid #ced4da',
      fontSize: 14,
      width: '120px',
      //padding: '10px 26px 10px 12px',
      padding: '5px 18px',
      transition: theme.transitions.create(['border-color', 'box-shadow']),
      // Use the system font instead of the default Roboto font.
      fontFamily: [
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
      '&:focus': {
        borderRadius: 4,
        borderColor: '#80bdff',
        boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
      },
    },
  }))(InputBase);
  


class KitchenDetails extends React.Component {

    
    
    state = {
                tabs_visibility: [true, false, false, false], 
                tabs_color : ["#1A1919","#B9B9B9","#B9B9B9","#B9B9B9"] 
            };  

    dropDown =  <FormControl >  
                    <NativeSelect value=""   input={<BootstrapInput name="none1s" id="non1s-customized-native-simple"  />}>
                        <option value="">Select Reason</option>
                    </NativeSelect>
                </FormControl>
    classes = styles;
    
    
    showTabs = (index) => {
       // alert(index);
        let tabs_visibility = this.state.tabs_visibility;
        let tabs_color = this.state.tabs_color;

        tabs_visibility.forEach((element,i) => {
            if(i!=index){
                tabs_visibility[i] = false;
                tabs_color[i] = "#B9B9B9";
            }else{
                tabs_visibility[i] = true;
                tabs_color[i] = "#1A1919";
            }
        });
       // this.setState({tabs_visibility: tabs_visibility});
        this.setState(({ tabs_visibility: tabs_visibility, tabs_color : tabs_color }));
        //alert(JSON.stringify(this.state.tabs_color));
    }


    render() {
            
        const {classes} = this.props;

        
            return (
              <div className={classes.header}>
                    <div style={{padding : '20px 0 0px'}}>
                    
                        <span className={classes.header_title1}>
                        Kitchen 1 - Chicago - IL -
                        </span>
                        <span className={classes.active_title}>
                            &nbsp;Active
                        </span>

                        <span className={classes.header_title2}>
                            Time Gap - 45 Min
                        </span>
                        
                    </div>
                    <Paper className={classes.root}>

                        <Table className={classes.table}>
                            <TableHead >
                        
                            <TableRow style={{border : '1px solid #d0c8c8'}} className={classes.tableHeight}>
                                <TableCell align="left" onClick={() => this.showTabs(0)} style={{color: this.state.tabs_color[0]}} className={classes.tableHead} >Account Info</TableCell>
                                <TableCell align="left" onClick={() => this.showTabs(1)} style={{color: this.state.tabs_color[1]}} className={classes.tableHead} >Serving Zip Codes</TableCell>
                                <TableCell align="left" onClick={() => this.showTabs(2)} style={{color: this.state.tabs_color[2]}}  className={classes.tableHead} >Operating Hour</TableCell>
                                <TableCell align="left" onClick={() => this.showTabs(3)} style={{color: this.state.tabs_color[3]}}  className={classes.tableHead} >Products</TableCell>
                            </TableRow>
                            </TableHead>
                        </Table>

                        {
                            (this.state.tabs_visibility[0]) && 
                            <AccountInfo/>
                        }

                        {
                            (this.state.tabs_visibility[1]) && 
                            <ServingZipCodes1/>
                        }

                        {
                            (this.state.tabs_visibility[2]) && 
                            <OperationHours/>
                        }

                    </Paper>
                </div>)
    }
  }
  

//ServingZipCodes.propTypes = {
 // classes: PropTypes.object.isRequired,
//};

export default withStyles(styles,{ withTheme: true })(KitchenDetails);
