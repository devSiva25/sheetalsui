import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
//import InboxIcon from '@material-ui/icons/MoveToInbox';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
//import MailIcon from '@material-ui/icons/Mail';
import MenuIcon from '@material-ui/icons/Menu';
import DashbaordIcon from '@material-ui/icons/Dashboard';
import BuildIcon from '@material-ui/icons/Build';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import RestaurantMenuIcon from '@material-ui/icons/RestaurantMenu';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';
import Button from '@material-ui/core/Button';
import ProductsTable from './products-table';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom'
//import ServingsTable from './kitchen-servings';
import ServingPackagesListView from './serving-packages-listeview';
import KitchenListView from './kitchen-list-view';
import KitchenDetails from './kitchen-details1';
import ProductAdd from './product-add';
import ServingPackagesOperation from './serving-packages-operation';
import ProductVisibility from './product-visibility';
import ProductPay from './product-pay';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import ServingPackagesTabs from './ServingPackagesTabs';
import styles from './../styles/side-bar';

const menus = [
    {
        text : "Dashboard",
        icon : <DashbaordIcon/>,
        linkTo : "/" 
    },
    {
        text : "Operations",
        icon : <BuildIcon />,
        linkTo : "operations"
    },
    {
        text : "Products",
        icon : <RestaurantMenuIcon/>,
        linkTo : "products"
    },
    {
      text : "Products 2",
      icon : <RestaurantMenuIcon/>,
      linkTo : "products-2"
  },
    {
        text : "Orders",
        icon : <ShoppingCartIcon/>,
        linkTo : "orders"
    },{
      text : "divider"
    },
    {
      text : "Serving Packages",
      icon : <ShoppingCartIcon/>,
      linkTo : "serving-packages"
    },
    {
      text : "Kitchens",
      icon : <ShoppingCartIcon/>,
      linkTo : "kitchens"
    },
    {
      text : "Promotions",
      icon : <ShoppingCartIcon/>,
      linkTo : "promotions"
    },
    {
      text : "Product 3",
      icon : <ShoppingCartIcon/>,
      linkTo : "products-3"
    },
    {		
      text : "Subscribers",		
      icon : <DashbaordIcon/>,		
      linkTo : "/" 		
    },		
    {		
        text : "Invitation",		
        icon : <DashbaordIcon/>,		
        linkTo : "/" 		
    },		
    {		
        text : "Settings",		
        icon : <DashbaordIcon/>,		
        linkTo : "/" 		
    },
]


class SideBar extends React.Component {
   
    content ="";

    menuColors= [];

  
    

    state = {
        mobileOpen: false,
      };
    
      handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
      };

      _this = this;
      changeColorActiveMenu = (menuIndex) => {
        //alert(menuIndex);
        this.menuColors[menuIndex] = this.props.menuTextActive;
      }
    
      render() {
        const { classes, theme } = this.props;
        const classAppBar = classes.appBar +" "+ classes.bgnBorder ;
        const classDrawPaper = classes.drawerPaper +" "+ classes.bgnBorder;
        const _this = this;
        
        menus.forEach(function(element,index){
          console.log(index);
          if(index ===0){
            _this.menuColors.push(classes.menuTextActive);
          }else{
            _this.menuColors.push(classes.menuText);
          }
        });

        const drawer = (
          <div>
           
            <div className={classes.toolbar} style={{height : '75px', borderBottom : '1px solid #b4b4b4'}}>
                <div className={classes.logoBox}> 
                    <span className={classes.logoText}>LOGO</span> 
                    <div className={classes.logoText}x>MasterAdmin</div>
                </div>
            </div>
            <Divider />
            <List className={classes.listMenu}>
               
                {menus.map( (menu,index) => (
                   
                   menu.text!=="divider" ?
                    <ListItem className={classes.menu_button} button key={menu.text}>
                        
                        <ListItemIcon></ListItemIcon>
                          <Link to={menu.linkTo} style={{textDecoration : 'none' }}>
                            <ListItemText  primary={menu.text} classes={{primary : this.menuColors[index] }} onClick={ () => this.changeColorActiveMenu(index)} />
                          </Link>
                        
                    </ListItem> 
                    :
                    <Divider className={classes.divider} />
                  
                ))};
            </List>
            
          </div>
        );
    
        return (
         
          <div className={classes.root}>
           <Router>
            <CssBaseline />
            <AppBar position="fixed" className={classAppBar}>
              <Toolbar style={{height : '75px'}}>
                <IconButton
                  color="inherit"
                  aria-label="Open drawer"
                  onClick={this.handleDrawerToggle}
                  className={classes.menuButton}
                >
                <MenuIcon />
                </IconButton>
                <Typography variant="div" color="inherit" style={{width:'auto'}} noWrap>
                    
                    
                    <InputBase className={classes.textField} placeholder="Search" classes={classes.textField} />
                    {/*
                    <IconButton className={classes.iconButton} aria-label="Search">
                        <SearchIcon />
                    </IconButton> */}
                    <Button variant="contained" color="primary" className={classes.button}>
                        Search
                    </Button>

                    {/*
                    <div className={classes.search}>
                        <div className={classes.searchIcon}>
                          <SearchIcon />
                        </div>
                        <InputBase placeholder="Search" classes={{
                                                              root: classes.inputRoot,
                                                              input: classes.inputInput,
                                                            }} />
                      </div>
                      */}

                      
                </Typography>
                
                <div className={classes.pullRight} style={{height: '16px', paddingTop:'2px', fontSize: '21px', position:'absolute', right:'5%'}}>
                        <span className={classes.userName}> Syed</span>
                        {/*
                        <span className={classes.location}> Master Admin</span>
                        */}
                      <Select  classes={{select : classes.user_dd}} value={1} style={{marginLeft : 10, padding : '0px', verticalAlign : 'bottom', width : 215, fontSize : 21}}  input={<Input name="group" id="group-helper" />}>
                        <MenuItem value={1}>California - San jose</MenuItem>
                        <MenuItem value={2}>Admin</MenuItem>
                        <MenuItem value={3}>Change Password</MenuItem>
                      </Select>
                </div>

                


              </Toolbar>
            </AppBar>
            <nav className={classes.drawer}>
              {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
              <Hidden smUp implementation="css">
                <Drawer
                  container={this.props.container}
                  variant="temporary"
                  anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                  open={this.state.mobileOpen}
                  onClose={this.handleDrawerToggle}
                  classes={{
                    paper: classDrawPaper
                  }}
                  
                >
                  {drawer}
                </Drawer>
              </Hidden>
              <Hidden xsDown implementation="css">
                <Drawer
                  classes={{
                    paper: classDrawPaper,
                  }}
                  variant="permanent"
                  open
                >
                  {drawer}
                </Drawer>
              </Hidden>
            </nav>
            
            <main className={classes.content}>
            <div className={classes.toolbar} />
                 <Route path="/products" component={ProductsTable} />
                 {/*
                 <Route path="/serving-packages1" component={ServingsTable} />
                 */}
                 <Route path="/kitchens" component={ServingPackagesListView} />
                 <Route path="/promotions" component={KitchenListView} />
                 <Route path="/kitchen-details" component={KitchenDetails} />
                 <Route path="/add-product" component={ProductAdd} /> 
                 <Route path="/operations" component={ServingPackagesOperation} /> 
                 <Route path="/products-2" component={ProductVisibility} />
                 <Route path="/products-3" component={ProductPay} />
                 <Route path="/serving-packages" component={ServingPackagesTabs} />
                 
            </main>
            </Router>
          </div>
          
        );
      }
}

SideBar.propTypes = {
    classes: PropTypes.object.isRequired,
    // Injected by the documentation to work in an iframe.
    // You won't need it on your project.
    container: PropTypes.object,
    theme: PropTypes.object.isRequired,
};
  
export default withStyles(styles, { withTheme: true })(SideBar);
  