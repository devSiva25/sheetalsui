import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import PackagePricing from './package-pricing';
import ComboSettings from './combo-settings';
import PackageStatus from './package-status';
import AddIcon from '@material-ui/icons/Add';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';
import InputBase from '@material-ui/core/InputBase';
import styles from './../styles/serving-packages-tabs';


const BootstrapInput = withStyles(theme => ({
    root: {
      'label + &': {
        marginTop: theme.spacing.unit * 3,
      }
    },
    input: {
      height : 30,
      color : '#707071',
      borderRadius: 3,
      position: 'relative',
      backgroundColor: theme.palette.background.paper,
      border: '1px solid rgba(112, 112, 112, 0.2)',
      fontSize: 20,
      //padding: '10px 26px 10px 12px',
      padding: '5px 20px',
      width : '155px',
      //padding: '5px 18px',
      transition: theme.transitions.create(['border-color', 'box-shadow']),
      // Use the system font instead of the default Roboto font.
      fontFamily: [
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
      '&:focus': {
        borderRadius: 4,
        borderColor: '#80bdff',
        boxShadow: '0px 3px 6px rgba(0, 0, 0, 0.16)',
      },
    },
  }))(InputBase);
      
function getSteps() {
  return ['Package Status', 'Package Pricing', 'Combo Setting'];
}

function getStepContent(step) {
  switch (step) {
    case 0:
      return <PackageStatus/>;
    case 1:
      return <PackagePricing/>;
    case 2:
      return <ComboSettings/>;
    default:
      return 'Unknown step';
  }
}

class ServingPackagesTabs extends React.Component {
  state = {
    activeStep: 0,
    skipped: new Set(),
  };

  

  isStepOptional = step => step === 1;

  handleNext = () => {
    const { activeStep } = this.state;
    let { skipped } = this.state;
    if (this.isStepSkipped(activeStep)) {
      skipped = new Set(skipped.values());
      skipped.delete(activeStep);
    }
    this.setState({
      activeStep: activeStep + 1,
      skipped,
    });
  };

  handleBack = () => {
    this.setState(state => ({
      activeStep: state.activeStep - 1,
    }));
  };

  handleSkip = () => {
    const { activeStep } = this.state;
    if (!this.isStepOptional(activeStep)) {
      // You probably want to guard against something like this,
      // it should never occur unless someone's actively trying to break something.
      throw new Error("You can't skip a step that isn't optional.");
    }

    this.setState(state => {
      const skipped = new Set(state.skipped.values());
      skipped.add(activeStep);
      return {
        activeStep: state.activeStep + 1,
        skipped,
      };
    });
  };

  handleReset = () => {
    this.setState({
      activeStep: 0,
    });
  };

  isStepSkipped(step) {
    return this.state.skipped.has(step);
  }

  render() {
    const { classes } = this.props;
    const steps = getSteps();
    const { activeStep } = this.state;

    const dropDown =  <FormControl >  
                        <NativeSelect value=""   input={<BootstrapInput name="none1s" id="non1s-customized-native-simple"  />}>
                            <option value="">Select Kitchen</option>
                        </NativeSelect>
                    </FormControl>

    return (
      <div className={classes.root}>
        <div className={classes.filterBox}>
            
            {/*
            <Button variant="contained" className={classes.button}>
                Refine By Kitchen
            </Button> */}

            {dropDown}
            
            <Button variant="contained" style={{float: 'right'}} className={classes.button}>
                Add Product &nbsp;
                <AddIcon className={classes.rightIcon} />
            </Button>
        </div>
        <Stepper activeStep={activeStep}>
          {steps.map((label, index) => {
            const props = {};
            const labelProps = {};
            if (this.isStepOptional(index)) {
              //labelProps.optional = <Typography variant="caption">Optional</Typography>;
            }
            if (this.isStepSkipped(index)) {
              props.completed = false;
            }
            return (
              <Step key={label} {...props}>
                <StepLabel {...labelProps}>{label}</StepLabel>
              </Step>
            );
          })}
        </Stepper>
        <div>
          {
            <div style={{padding : "0 2rem"}}>
              <Typography className={classes.instructions}>{getStepContent(activeStep)}</Typography>
              <div style={{textAlign : 'center'}}>
                <Button
                  disabled={activeStep === 0}
                  onClick={this.handleBack}
                  className={classes.cancelButton}
                  variant="outlined"
                >
                  Back
                </Button>
                
                <Button
                  vvariant="outlined"
                  onClick={this.handleNext}
                  className={classes.saveButton}
                >
                  {activeStep === steps.length - 1 ? 'Save' : 'Next' }
                  
                </Button>
              </div>
            </div>
          }
        </div>
      </div>
    );
  }
}

ServingPackagesTabs.propTypes = {
  classes: PropTypes.object,
};

export default withStyles(styles)(ServingPackagesTabs);
