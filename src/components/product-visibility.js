import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';
import {styles, BootstrapInput2, BootstrapInput} from './../styles/products-visibility';





function ProductVisibility(props) {
  const { classes } = props;
  
  const dropDown =  <FormControl >  
                        <NativeSelect value=""   input={<BootstrapInput name="none1s" id="non1s-customized-native-simple"  />}>
                            <option value="">2 Days</option>
                        </NativeSelect>
                    </FormControl>

    const dropDown1 =  <FormControl style={{width  : '100%'}}>  
                        <NativeSelect value=""   input={<BootstrapInput2 name="none1s" id="non1s-customized-native-simple"  />}>
                            <option value="">2 Days</option>
                        </NativeSelect>
                    </FormControl>

    const dropDown2 =  <FormControl style={{width  : '100%'}}>  
                        <NativeSelect value=""   input={<BootstrapInput2 name="none1s" id="non1s-customized-native-simple"  />}>
                            <option value="">Kitchen</option>
                        </NativeSelect>
                     </FormControl>
  //const weekddays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
  return (
    <div>
            <div className={classes.contentBox}>
                <div style={{marginBottom : '20px', padding : '20px 2rem', borderTop: '1px solid rgba(112, 112, 112, 0.5)', borderBottom: '1px solid rgba(112, 112, 112, 0.5)'}}>
                    <span style={{color: '#000', fontWeight: 600, display : 'inline-block', 'marginRight' : '10px' }}>
                    Product Detail &nbsp;|&nbsp;
                    </span>

                    <span style={{color: '#2C90F3', fontWeight: 600, display : 'inline-block', 'marginRight' : '10px' }}>
                        Visibility by Days & Kitchen  &nbsp;
                    </span>

                    <span style={{color: '#000', fontWeight: 600, display : 'inline-block', 'marginRight' : '10px' }}>
                    |&nbsp; Pay as you go
                    </span>

                    <Button variant="contained" style={{float : 'right', margin : '-4px 0'}} className={classes.button} >
                    + Add Product
                    </Button>
                </div>
                <div style={{float:'left', 'width': '100%', padding : '20px 2rem'}}>
                    <div className={classes.leftContent}>
                            Wild Alaska pollock & quinto with tomato sauce
                            <br/>
                            <span style={{fontSize : '12px', fontWeight : 500, color :'#3E3F40'}}> 
                            Product ID - 556677 Menu type - Main course
                            </span>
                    </div>
                    <div  className={classes.leftContent}>
                        How many days this product can be served by one kitchen in a week
                    </div>

                    <div  className={classes.leftContent}>
                        {dropDown}
                    </div>
                    <div className={classes.contentTable}>
                        <Table className={classes.table}>
                                <TableHead>
                                <TableRow className={classes.tableHead}>
                                    <TableCell>Days</TableCell>
                                    <TableCell align="left" className={classes.headCell}>Serving Kitchens</TableCell>
                                    <TableCell align="left" className={classes.headCell}>Excluded Zip Code By Kitchen</TableCell>
                                    <TableCell align="left" className={classes.headCell}>Kitchen</TableCell>
                                </TableRow>
                                </TableHead>
                                <TableBody>
                                    <TableRow key="1" className={classes.tableBodyRow}>
                                        <TableCell align="left" className={classes.cellDays}>Monday</TableCell>
                                        <TableCell align="left" className={classes.cellBorder}>K1,K2,K3,K4</TableCell>
                                        <TableCell align="left" className={classes.cellBorder}>K1-60645, K2-7889,</TableCell>
                                        <TableCell align="left" className={classes.cellBorder}>Remove v</TableCell>
                                    </TableRow>
                                    <TableRow key="2" className={classes.tableBodyRow}>
                                        <TableCell align="left" className={classes.cellDays}>Tuesday</TableCell>
                                        <TableCell align="left" className={classes.cellBorder}> K1,K2,K3,K4</TableCell>
                                        <TableCell align="left" className={classes.cellBorder}>K1-60645, K2-7889,</TableCell>
                                        <TableCell align="left" className={classes.cellBorder}>Remove v</TableCell>
                                    </TableRow>
                                    <TableRow key="3" className={classes.tableBodyRow}>
                                        <TableCell align="left" className={classes.cellDays}>Wednesday</TableCell>
                                        <TableCell align="left" className={classes.cellBorder}> K1,K2,K3,K4</TableCell>
                                        <TableCell align="left" className={classes.cellBorder}>K1-60645, K2-7889,</TableCell>
                                        <TableCell align="left" className={classes.cellBorder}>Remove v</TableCell>
                                    </TableRow>
                                    <TableRow key="4" className={classes.tableBodyRow}>
                                        <TableCell align="left" className={classes.cellDays}>Thursday</TableCell>
                                        <TableCell align="left" className={classes.cellBorder}> K1,K2,K3,K4</TableCell>
                                        <TableCell align="left" className={classes.cellBorder}>K1-60645, K2-7889,</TableCell>
                                        <TableCell align="left" className={classes.cellBorder}>Remove v</TableCell>
                                    </TableRow>
                                    <TableRow key="5" className={classes.tableBodyRow}>
                                        <TableCell align="left" className={classes.cellDays}>Friday</TableCell>
                                        <TableCell align="left" className={classes.cellBorder}> K1,K2,K3,K4</TableCell>
                                        <TableCell align="left" className={classes.cellBorder}>K1-60645, K2-7889,</TableCell>
                                        <TableCell align="left" className={classes.cellBorder}>Remove v</TableCell>
                                    </TableRow>
                                    <TableRow key="6" className={classes.tableBodyRow}>
                                        <TableCell align="left" className={classes.cellDays}>Saturday</TableCell>
                                        <TableCell align="left" className={classes.cellBorder}> K1,K2,K3,K4</TableCell>
                                        <TableCell align="left" className={classes.cellBorder}>K1-60645, K2-7889,</TableCell>
                                        <TableCell align="left" className={classes.cellBorder}>Remove v</TableCell>
                                    </TableRow>
                                    <TableRow key="7" className={classes.tableBodyRow}>
                                        <TableCell align="left" className={classes.cellDays}>Sunday</TableCell>
                                        <TableCell align="left" className={classes.cellBorder}> K1,K2,K3,K4</TableCell>
                                        <TableCell align="left" className={classes.cellBorder}>K1-60645, K2-7889,</TableCell>
                                        <TableCell align="left" className={classes.cellBorder}>Remove v</TableCell>
                                    </TableRow>
                                </TableBody>
                            </Table>
                    </div>
                    <div className={classes.contentTable}>
                        <div>
                        <Button variant="contained" style={{background : '#727272'}} className={classes.button} >
                            + Add Special Rules
                        </Button>
                        </div>
                        <div className={classes.floatLeft}>
                            <Table className={classes.table2}>
                                <TableHead>
                                <TableRow className={classes.tableHead}>
                                    <TableCell className={classes.table2Cells}>Kitchen Name</TableCell>
                                    <TableCell align="left" className={classes.table2Cells} >Days Allocated to serve this item per week</TableCell>
                                    <TableCell align="left" className={classes.table2Cells}>Action</TableCell>
                                </TableRow>
                                </TableHead>
                                <TableBody>
                                    <TableRow key="1" className={classes.tableBodyRow}>
                                        <TableCell align="left" className={classes.table2Cells}>Monday</TableCell>
                                        <TableCell align="left" className={classes.table2Cells}>K1,K2,K3,K4</TableCell>
                                        <TableCell align="left" className={classes.table2Cells}>K1-60645, K2-7889,</TableCell>
                                    </TableRow>
                                </TableBody>
                            </Table>
                        </div>

                        <div className={classes.floatRight}>
                            <div className={classes.details}>
                                Allocate extra serving days per week to kitchen
                            </div>
                            <div className={classes.details}>
                                {dropDown1}
                            </div>
                            <div className={classes.details} >
                                {dropDown2}
                            </div>
                            <div className={classes.details} >
                            <Button variant="contained" color="primary" className={classes.saveButton}>
                                Save
                            </Button>
                            </div>

                            <div className={classes.details} >
                                <Button variant="contained" color="primary" className={classes.cancelBtn}>
                                    Cancel
                                </Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
  );
}

ProductVisibility.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProductVisibility);
