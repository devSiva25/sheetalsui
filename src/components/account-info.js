import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';

import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';
import InputBase from '@material-ui/core/InputBase';
import styles from './../styles/account-info'

let id = 0;
function createData(name, city, state, status, zipcodes, orderscap, drivers, subscribers, products, lastwkorder) {
  id += 1;
  return { name, city, state, status, zipcodes, orderscap, drivers, subscribers, products, lastwkorder };
}

const rows = [
  createData('K1', "Chicago" , "IL", "Active", 70, 2000, 50, 5367, 70, 1010 ),
];


const BootstrapInput = withStyles(theme => ({
    root: {
      'label + &': {
        marginTop: theme.spacing.unit * 3,
      },
    },
    input: {
      borderRadius: 4,
      position: 'relative',
      backgroundColor: theme.palette.background.paper,
      border: '1px solid #ced4da',
      fontSize: 16,
      width: '120px',
      //padding: '10px 26px 10px 12px',
      padding: '5px 18px',
      transition: theme.transitions.create(['border-color', 'box-shadow']),
      // Use the system font instead of the default Roboto font.
      fontFamily: [
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
      '&:focus': {
        borderRadius: 4,
        borderColor: '#80bdff',
        boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
      },
    },
  }))(InputBase);
  

function AccountInfo(props) {
  const { classes } = props;
  
  const dropDown =  <FormControl >  
                        <NativeSelect value=""   input={<BootstrapInput name="none1s" id="non1s-customized-native-simple"  />}>
                            <option value="">Select Reason</option>
                        </NativeSelect>
                    </FormControl>
  
  return (
    <div>
            <div className={classes.contentBox}>
                <div className={classes.floatLeft}>
                    <Table style={{maxWidth : 'auto'}} className={classes.table2}>
                        <TableBody >
                            <TableRow className={classes.tableHeight2}>
                                <TableCell align="left" className={classes.detail_cell} >Account Info :</TableCell>
                                <TableCell align="left" className={classes.table2Cells} style={{color: '#B9B9B9'}}>Syed Ikram</TableCell>
                            </TableRow>
                            <TableRow className={classes.tableHeight2}>
                                <TableCell align="left" className={classes.detail_cell} >Email Address :</TableCell>
                                <TableCell align="left" className={classes.table2Cells} style={{color: '#B9B9B9'}}>syedhaq@gmail.com</TableCell>
                            </TableRow>
                            <TableRow className={classes.tableHeight2}>
                                <TableCell align="left" className={classes.detail_cell} >Contact No :</TableCell>
                                <TableCell align="left" className={classes.table2Cells} style={{color: '#B9B9B9'}}>+12432131</TableCell>
                            </TableRow>
                            <TableRow  className={classes.tableHeight2}>
                                <TableCell align="left" className={classes.detail_cell} >Account Info :</TableCell>
                                <TableCell align="left" className={classes.table2Cells} style={{color: '#B9B9B9'}}>Syed Ikram</TableCell>
                            </TableRow>

                            <TableRow  className={classes.tableHeight2}>
                                <TableCell align="left" className={classes.detail_cell}>&nbsp;</TableCell>
                                <TableCell align="left" className={classes.table2Cells} style={{color: '#B9B9B9'}}>&nbsp;</TableCell>
                            </TableRow>

                            <TableRow className={classes.tableHeight2}>
                                <TableCell align="left" className={classes.detail_cell} >Account Info :</TableCell>
                                <TableCell align="left" className={classes.table2Cells} style={{color: '#B9B9B9'}}>Syed Ikram</TableCell>
                            </TableRow>
                            <TableRow className={classes.tableHeight2}>
                                <TableCell align="left" className={classes.detail_cell} >Email Address :</TableCell>
                                <TableCell align="left" className={classes.table2Cells} style={{color: '#B9B9B9'}}>syedhaq@gmail.com</TableCell>
                            </TableRow>
                            <TableRow className={classes.tableHeight2}>
                                <TableCell align="left" className={classes.detail_cell} >Contact No :</TableCell>
                                <TableCell align="left" className={classes.table2Cells} style={{color: '#B9B9B9'}}>+12432131</TableCell>
                            </TableRow>
                            <TableRow  className={classes.tableHeight2}>
                                <TableCell align="left" className={classes.detail_cell} >Account Info :</TableCell>
                                <TableCell align="left" className={classes.table2Cells} style={{color: '#B9B9B9'}}>Syed Ikram</TableCell>
                            </TableRow>
                            <TableRow className={classes.tableHeight2}>
                                <TableCell align="left" className={classes.detail_cell} >Email Address :</TableCell>
                                <TableCell align="left" className={classes.table2Cells} style={{color: '#B9B9B9'}}>syedhaq@gmail.com</TableCell>
                            </TableRow>
                            <TableRow className={classes.tableHeight2}>
                                <TableCell align="left" className={classes.detail_cell} >Contact No :</TableCell>
                                <TableCell align="left" className={classes.table2Cells} style={{color: '#B9B9B9'}}>+12432131</TableCell>
                            </TableRow>
                            <TableRow  className={classes.tableHeight2}>
                                <TableCell align="left" className={classes.detail_cell} >Account Info :</TableCell>
                                <TableCell align="left" className={classes.table2Cells} style={{color: '#B9B9B9'}}>Syed Ikram</TableCell>
                            </TableRow>

                        </TableBody>
                    </Table>
                    <div className={classes.buttonBox}>
                        <Button variant="contained" className={classes.login}>
                            Login To Kitchen Portal
                        </Button>
                    </div>
                </div>
                <div className={classes.floatLeft}>
                        <div className={classes.headingLabel}> 
                            Account Settings
                        </div>

                        <div className={classes.contentBox2}>
                            <Table style={{maxWidth : '400px'}} className={classes.table2}>
                                <TableBody >
                                    <TableRow className={classes.tableHeight}>
                                        <TableCell align="right" className={classes.table2Cells}>Kitchen Type</TableCell>
                                        <TableCell align="left" className={classes.table2Cells}>
                                            <input type="radio" id="accType" name="accType" checked />
                                            <label for="accType">Combo</label>
                                            &nbsp;
                                            <input type="radio" id="accType" name="accType" />
                                            <label for="accType">PAYG</label>
                                        </TableCell>
                                    </TableRow>
                                    <TableRow className={classes.tableHeight}>
                                        <TableCell align="right" className={classes.table2Cells}>Status</TableCell>
                                        <TableCell align="left" className={classes.table2Cells}>
                                        <Button variant="contained" style={{border: '1px solid #0BBE0B', color : '#0BBE0B'}} className={classes.statusButton}>
                                            Active
                                        </Button>
                                        <Button variant="contained" color="primary" className={classes.statusButton}>
                                            Inactive
                                        </Button>
                                        </TableCell>
                                    </TableRow>
                                    <TableRow className={classes.tableHeight}>
                                        <TableCell align="right" className={classes.table2Cells}>Select Reason</TableCell>
                                        <TableCell align="left" className={classes.table2Cells}>
                                        {dropDown}
                                        </TableCell>
                                    </TableRow>
                                    <TableRow className={classes.tableHeight}>
                                        <TableCell align="right" className={classes.table2Cells}>&nbsp;</TableCell>
                                        <TableCell align="left" className={classes.table2Cells}>
                                            <div style={{padding : '20px 0'}}>
                                                <Button className={classes.cancelBtn} variant="outlined" >
                                                    Cancel
                                                </Button>
                                                <Button className={classes.saveBtn} variant="outlined" >
                                                    Save
                                                </Button>
                                            </div>
                                        </TableCell>
                                    </TableRow>
                                </TableBody>
                            </Table>
                        </div>
                </div>
            </div>
    </div>
  );
}

AccountInfo.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AccountInfo);
