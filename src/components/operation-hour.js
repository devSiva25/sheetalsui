import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import styles from './../styles/operation-hour'



let id = 0;
function createData(timeSlot, orderLimits, rules) {
  id += 1;
  return { id, timeSlot, orderLimits, rules};
}

const rows = [
  createData("12:15 PM - 01:15 PM CST", "----", "60645(5),60076(50)"),
  createData("12:15 PM - 01:15 PM CST", "----", "60645(5),60076(50)" )
];


function OperationHours(props) {
  const { classes } = props;
  
  return (
    <div>
    
    <Paper className={classes.root}>
    
      
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell className={classes.tableHeadCell}>Time Slot</TableCell>
            <TableCell className={classes.tableHeadCell} align="left">Order Limits</TableCell>
            <TableCell className={classes.tableHeadCell} align="left">Special Order rule by zip code</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map(row => (
            <TableRow key={row.id}>
              <TableCell className={classes.tableBodyCell} component="th" scope=" row">
                {row.timeSlot}
              </TableCell>
              <TableCell className={classes.tableBodyCell} align="left">{row.orderLimits}</TableCell>
              <TableCell className={classes.tableBodyCell}  align="left">{row.rules}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
    </div>
  );
}

OperationHours.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(OperationHours);
