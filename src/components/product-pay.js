import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import Radio from '@material-ui/core/Radio';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';
import {styles, BootstrapInput} from './../styles/product-pay';

function ProductPay(props) {
  const { classes } = props;
  
  const dropDown =  <FormControl >  
                        <NativeSelect value=""   input={<BootstrapInput name="none1s" id="non1s-customized-native-simple"  />}>
                            <option value="">0.5%</option>
                        </NativeSelect>
                    </FormControl>

    const dropDown1 =  <FormControl>  
                        <NativeSelect value=""   input={<BootstrapInput name="none1s" id="non1s-customized-native-simple"  />}>
                            <option value="">60645</option>
                        </NativeSelect>
                    </FormControl>

    const dropDown2 =  <FormControl>  
                        <NativeSelect value=""   input={<BootstrapInput name="none1s" id="non1s-customized-native-simple"  />}>
                            <option value="">$6.89</option>
                        </NativeSelect>
                     </FormControl>
  
  return (
    <div>


        
            <div className={classes.contentBox}>
                <div style={{marginBottom : '20px', padding : '20px', borderTop: '1px solid rgba(112, 112, 112, 0.5)', borderBottom: '1px solid rgba(112, 112, 112, 0.5)'}}>
                    <span style={{color: '#000', fontWeight: 600, display : 'inline-block', 'marginRight' : '10px' }}>
                    Product Detail &nbsp;|&nbsp;
                    </span>

                    <span style={{color: '#2C90F3', fontWeight: 600, display : 'inline-block', 'marginRight' : '10px' }}>
                        Visibility by Days & Kitchen  &nbsp;
                    </span>

                    <span style={{color: '#000', fontWeight: 600, display : 'inline-block', 'marginRight' : '10px' }}>
                    |&nbsp; Pay as you go
                    </span>

                    <Button variant="contained" style={{float : 'right', margin : '-8px 0'}} className={classes.button} >
                    + Add Product
                    </Button>
                </div>
                <div style={{float:'left', 'width': '100%', padding : '20px'}}>
                    <div className={classes.leftContent}>
                            Wild Alaska pollock & quinto with tomato sauce
                            <br/>
                            <span style={{fontSize : '12px', fontWeight : 500, color :'#3E3F40'}}> 
                            Product ID - 556677 Menu type - Main course
                            </span>
                    </div>
                    <div  className={classes.leftContent} style={{fontWeight : '500'}}>
                        Set percentage to increase or decrease display price
                    </div>

                    <div  className={classes.leftContent}>
                        {dropDown}
                    </div>
                  
                        <Button variant="contained" style={{float : 'right',  border: '1px solid #e0e0e0'}} className={classes.button} >
                        + Add Product
                        </Button>
                    <div className={classes.contentTable}>
                        <Table className={classes.table}>
                                <TableHead>
                                <TableRow className={classes.tableHead}>
                                    <TableCell >Select</TableCell>
                                    <TableCell align="left" className={classes.headCell}>Kitchen Name</TableCell>
                                    <TableCell align="left" className={classes.headCell}>Kitchen Type</TableCell>
                                    <TableCell align="left" className={classes.headCell}>Kitchen Cost</TableCell>
                                    <TableCell align="left" className={classes.headCell}>Price for customer</TableCell>
                                    <TableCell align="left" className={classes.headCell}>Select Zip Code Price</TableCell>
                                    <TableCell align="left" className={classes.headCell}>Updated By</TableCell>
                                </TableRow>
                                </TableHead>
                                <TableBody>
                                    <TableRow key="1" className={classes.tableBodyRow}>
                                        <TableCell align="left" className={classes.noBorder}>
                                         <Radio  checked="true"  value="a" name="radio-button-demo" aria-label="A" style={{padding : '0'}}/>
                                        </TableCell>
                                        <TableCell align="left" className={classes.noBorder}>K1</TableCell>
                                        <TableCell align="left" className={classes.noBorder}>Combo</TableCell>
                                        <TableCell align="left" className={classes.noBorder}>$5.59</TableCell>
                                        <TableCell align="left" className={classes.noBorder}>$5.59</TableCell>
                                        <TableCell align="left" className={classes.noBorder}>K1-60645($3.99),7080($2.99)</TableCell>
                                        <TableCell align="left" className={classes.noBorder}>System</TableCell>
                                    </TableRow>
                                    <TableRow key="1" className={classes.tableBodyRow}>
                                        <TableCell align="left" className={classes.noBorder}>
                                         <Radio  checked={false}  value="a" name="radio-button-demo" aria-label="A" style={{padding : '0'}}/>
                                        </TableCell>
                                        <TableCell align="left" className={classes.noBorder}>K1</TableCell>
                                        <TableCell align="left" className={classes.noBorder}>Combo</TableCell>
                                        <TableCell align="left" className={classes.noBorder}>$5.59</TableCell>
                                        <TableCell align="left" className={classes.noBorder}>$5.59</TableCell>
                                        <TableCell align="left" className={classes.noBorder}>K1-60645($3.99),7080($2.99)</TableCell>
                                        <TableCell align="left" className={classes.noBorder}>System</TableCell>
                                    </TableRow>
                                </TableBody>
                            </Table>
                    </div>
                    <div className={classes.contentTable}>
                        <div className={classes.floatRight}>
                                <div className={classes.details} style={{color : '#F5B22B'}}>
                                    Allocate extra serving days per week to kitchen
                                </div>
                                <div className={classes.details}>
                                    <div>Kitchen - K1 </div>
                                    <div>Kitchen Cost - $5.99</div>
                                    <div>Customer Price</div>
                                </div>

                                <div className={classes.details} >
                                    + Special Zip Code
                                </div>
                                
                                <div className={classes.details} >
                                    {dropDown2}&nbsp;{dropDown1} &nbsp;
                                    <span style={{verticalAlign : 'middle'}}>Remove</span>
                                </div>
                          
                                <div className={classes.details} >
                                <Button variant="contained" color="primary" className={classes.saveButton}>
                                    Save
                                </Button>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
    </div>
  );
}

ProductPay.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProductPay);
