import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import  {styles} from './../styles/kitchen-serving-zipcodes';


let id = 0;
function createData(name, calories, fat, carbs, protein, kitchen) {
  id += 1;
  return { id, name, calories, fat, carbs, protein, ...kitchen };
}

const rows = [
  createData('IL', 'Stokie', "Cook", "60076", 58, ["10.3 Mile", "Active", "Open", "5-5.30(0), 5-5.6.30(0)"] ),
  createData('IL', 'Stokie', "Cook", "60076", 58, ["10.3 Mile", "Active", "Open", "5-5.30(0), 5-5.6.30(0)"] )
];

function ServingZipCodes1(props) {
  const { classes } = props;
 
  return (
    <div>
    
    <Paper className={classes.root}>
    
      
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell className={classes.tableHeadCell}>State</TableCell>
            <TableCell className={classes.tableHeadCell} align="left">City</TableCell>
            <TableCell className={classes.tableHeadCell} align="left">Country</TableCell>
            <TableCell className={classes.tableHeadCell} align="left">Zip Code</TableCell>
            <TableCell className={classes.tableHeadCell} align="left"># Subscribers</TableCell>
            <TableCell className={classes.tableHeadCell} align="left">Distance</TableCell>
            <TableCell className={classes.tableHeadCell} align="left">Kitchen Status</TableCell>
            <TableCell className={classes.tableHeadCell} align="left">Admin Zip Status</TableCell>
            <TableCell className={classes.tableHeadCell} align="left">Special Order Rules</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map(row => (
            <TableRow key={row.id}>
              <TableCell component="th" scope=" row">
                {row.name}
              </TableCell>
              <TableCell className={classes.tableBodyCell} align="left">{row.calories}</TableCell>
              <TableCell className={classes.tableBodyCell}  align="left">{row.fat}</TableCell>
              <TableCell className={classes.tableBodyCell}  align="left">{row.carbs}</TableCell>
              <TableCell className={classes.tableBodyCell}  align="left">{row.protein}</TableCell>
              <TableCell className={classes.tableBodyCell}  align="left">{row[0] }</TableCell>
              <TableCell className={classes.tableBodyCell}  align="left">{row[1] }</TableCell>
              <TableCell className={classes.tableBodyCell}  align="left">{row[2] }</TableCell>
              <TableCell className={classes.tableBodyCell}  align="left">{row[3] }</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
    </div>
  );
}

ServingZipCodes1.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ServingZipCodes1);
