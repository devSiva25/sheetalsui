import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';
import {styles, BootstrapInput} from './../styles/products-table'

let id = 0;
function createData(img, type, name, pid, available,  kitchen) {
  id += 1;
  return { id, img, type, name, pid, available,...kitchen };
}

const rows = [
  createData( "" , "Main Course",  'Frozen yoghurt', 159, "Avaiable", ["K1", "K2", "K3", "K4"] ),
  createData( "" , "Main Course",  'Frozen yoghurt', 159, "Avaiable", ["K1", "K2", "K3", "K4"] ),
  createData( "" , "Main Course",  'Frozen yoghurt', 159, "Avaiable", ["K1", "K2", "K3", "K4"] ),
  createData( "" , "Main Course",  'Frozen yoghurt', 159, "Avaiable", ["K1", "K2", "K3", "K4"] ),
  createData( "" , "Main Course",  'Frozen yoghurt', 159, "Avaiable", ["K1", "K2", "K3", "K4"] ),
];

console.log(rows);

function ProductsTable(props) {
  const { classes } = props;
  const weekddays = ["Sun", "Mon", "Tue", "Wed"];

  const handleClick = function(props){
                        alert("clicked");
                        props.router.push("/HomePage");
                      }
  
    const refinKitchenDD =  <FormControl >  
        <NativeSelect value=""   input={<BootstrapInput name="none1s" id="non1s-customized-native-simple1"  />}>
            <option value="">Refine By Kitchen</option>
        </NativeSelect>
      </FormControl>
    
    const refineMenuTypeDD =  <FormControl >  
        <NativeSelect value=""   input={<BootstrapInput name="none1s" id="non1s-customized-native-simple2"  />}>
            <option value="">Refine By Menu Type</option>
        </NativeSelect>
      </FormControl>
    
    const specialMarkingDD =  <FormControl >  
                              <NativeSelect value=""   input={<BootstrapInput name="none1s" id="non1s-customized-native-simple3"  />}>
                                  <option value="">Special Marking</option>
                              </NativeSelect>
                            </FormControl>

  return (
    <div style={{padding:'2rem'}} >
    <div style={{textAlign:'left'}}>
        <Button variant="contained" className={classes.button}>
            Refine By Date
        </Button>&nbsp;&nbsp;
        {/*
        <Button variant="contained" className={classes.button}>
            Refine By Kitchen
        </Button>*/}
        {refinKitchenDD} &nbsp;&nbsp;
        {/*
        <Button variant="contained" className={classes.button}>
            Refine By Menu Type
        </Button> */}
        {refineMenuTypeDD}&nbsp;&nbsp;
        {/*
        <Button variant="contained" className={classes.button}>
           Special Marking
        </Button> */}
        {specialMarkingDD}&nbsp;&nbsp;
        <Button variant="contained" className={classes.button} onClick={handleClick}>
          + Add Product
        </Button>
    </div>
    <Paper className={classes.root}>
    
      
      <Table className={classes.table}>
        <TableHead>
          <TableRow className={classes.headRowHeight}>
            <TableCell className={classes.headCellPadding}>Image</TableCell>
            <TableCell align="left" className={classes.headCellPadding}>Menu Type</TableCell>
            <TableCell align="left" className={classes.headCellPadding}>Product Name</TableCell>
            <TableCell align="left" className={classes.headCellPadding}>Product ID</TableCell>
            <TableCell align="left" className={classes.headCellPadding}>Available Status</TableCell>
            {
                weekddays.map( (text,index) => (
                    <TableCell className={classes.headCellPadding}>{text}</TableCell>
                ))
            }
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map(row => (
            <TableRow key={row.id} className={classes.bodyRow}>
              <TableCell  className={classes.cellPadding} component="th" scope="row">
                {row.img}
              </TableCell>
              <TableCell align="left" className={classes.cellPadding}>{row.type}</TableCell>
              <TableCell align="left" className={classes.cellPadding}>{row.name}</TableCell>
              <TableCell align="left" className={classes.cellPadding}>{row.pid}</TableCell>
              <TableCell align="left" className={classes.cellPadding}>{row.available}</TableCell>
              <TableCell align="left" className={classes.cellPadding}>{row[0] }</TableCell>
              <TableCell align="left" className={classes.cellPadding}>{row[1] }</TableCell>
              <TableCell align="left" className={classes.cellPadding}>{row[2] }</TableCell>
              <TableCell align="left" className={classes.cellPadding} >{row[3] }</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
    </div>
  );
}

ProductsTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProductsTable);
