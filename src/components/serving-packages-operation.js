import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Radio from '@material-ui/core/Radio';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';
import InputBase from '@material-ui/core/InputBase';
import Switch from '@material-ui/core/Switch';
import {styles} from '../styles/serving-packages-operation'



let id = 0;
function createData(name, calories, fat, carbs, protein, kitchen) {
  id += 1;
  return { id, name, calories, fat, carbs, protein, ...kitchen };
}

const rows = [
  createData('IL', 'Stokie', "Cook", "60076", 58, ["K1","10.3 Mile", "Active", "Open", "5-5.30(0), 5-5.6.30(0)"] ),
  createData('IL', 'Stokie', "Cook", "60076", 58, ["K1","10.3 Mile", "Inactive", "Closed", "5-5.30(0), 5-5.6.30(0)"] ),
  createData('IL', 'Stokie', "Cook", "60076", 58, ["K1","10.3 Mile", "Inactive", "Closed", "5-5.30(0), 5-5.6.30(0)"] )
];

class ServingPackagesOperation extends React.Component {
  
  
  showTab(){

  }
  

  render() {
    const { classes } = this.props;
    const BootstrapInput = withStyles(theme => ({
        root: {
          'label + &': {
            marginTop: theme.spacing.unit * 3,
          }
        },
        input: {
          height : 30,
          color : '#707071',
          borderRadius: 3,
          position: 'relative',
          backgroundColor: theme.palette.background.paper,
          border: '1px solid rgba(112, 112, 112, 0.2)',
          fontSize: 12,
          //padding: '10px 26px 10px 12px',
          padding: '5px 20px',
          width : '155px',
          //padding: '5px 18px',
          transition: theme.transitions.create(['border-color', 'box-shadow']),
          // Use the system font instead of the default Roboto font.
          fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
          ].join(','),
          '&:focus': {
            borderRadius: 4,
            borderColor: '#80bdff',
            boxShadow: '0px 3px 6px rgba(0, 0, 0, 0.16)',
          },
        },
      }))(InputBase);
    const dropDown =  <FormControl >  
                    <NativeSelect value=""   input={<BootstrapInput name="none1s" id="non1s-customized-native-simple"  />}>
                        <option value="">Refine By Kitchen</option>
                    </NativeSelect>
                </FormControl>
      
     
      const dropDown2 =  <FormControl style={{marginLeft : '20px'}} >  
                    <NativeSelect value=""   input={<BootstrapInput name="none1s" id="non1s-customized-native-simple"  />}>
                        <option value="">Refine By Kitchen</option>
                    </NativeSelect>
                </FormControl>

      
      const dropDown3 =  <FormControl style={{marginLeft : '20px'}} >  
                          <NativeSelect value=""   input={<BootstrapInput name="none1s" id="non1s-customized-native-simple"  />}>
                              <option value="">Refine Kitchen Zip Status</option>
                          </NativeSelect>
                          </FormControl>

      return(
    <div style={{padding:'4rem 2rem 2rem', backgroundColor : '#fff'}}>
     <div className={classes.filterBox}>
            <span className={classes.heading}>
                Serving Zip Codes
            </span>
            {dropDown2}
            {dropDown3}

            <div className={classes.pullRight}>
            <span className={classes.btnLabel}> Action </span>
            <Button variant="contained" style={{float: 'right'}} className={classes.actionButton}>
                Allocate Zip To Kitchen
            </Button>
            </div>
        </div>
          
    <Paper className={classes.root}>

   
      <Table className={classes.table}>
        <TableHead >
          <TableRow style={{height: '35px', background : 'whitesmoke'}}>
            <TableCell className={classes.tableHeadCell} align="left">Select</TableCell>
            <TableCell className={classes.tableHeadCell} align="left">State</TableCell>
            <TableCell className={classes.tableHeadCell} align="left">City</TableCell>
            <TableCell className={classes.tableHeadCell} align="left">Country</TableCell>
            <TableCell className={classes.tableHeadCell} align="left">Zip Code</TableCell>
            <TableCell className={classes.tableHeadCell} align="left"># Subscribers</TableCell>
            <TableCell className={classes.tableHeadCell} align="left">Serving Kitchen</TableCell>
            <TableCell className={classes.tableHeadCell} align="left">Distance</TableCell>
            <TableCell className={classes.tableHeadCell} align="left">Kitchen Status</TableCell>
            <TableCell className={classes.tableHeadCell} align="left">Admin Zip Status</TableCell>
            <TableCell className={classes.tableHeadCell} align="left">Special Order Rules</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map(row => (
            <TableRow key={row.id} style={{height: '30px'}}>
              <TableCell className={classes.tableBodyCell} >
                <Radio  checked="true"  value="a" name="radio-button-demo" aria-label="A" style={{padding : 0, color : '#C2DCF1'}}/>
              </TableCell>
              <TableCell className={classes.tableBodyCell}  component="th" scope=" row">
                {row.name}
              </TableCell>
              <TableCell className={classes.tableBodyCell} align="left">{row.calories}</TableCell>
              <TableCell className={classes.tableBodyCell}  align="left">{row.fat}</TableCell>
              <TableCell className={classes.tableBodyCell}  align="left">{row.carbs}</TableCell>
              <TableCell className={classes.tableBodyCell}  align="left">{row.protein}</TableCell>
              <TableCell className={classes.tableBodyCell}  align="left">{row[0] }</TableCell>
              <TableCell className={classes.tableBodyCell}  align="left">{row[1] }</TableCell>
             

              { row[2] === "Inactive" &&
              <TableCell className={classes.tableBodyCell} style={{color : '#71f494'}}  align="left">{row[2] }</TableCell> }
              { row[2] === "Active" &&
              <TableCell className={classes.tableBodyCell} style={{color : '#fb6973'}} align="left">{row[2] }</TableCell> }

              { row[3] === "Closed" &&
              <TableCell className={classes.tableBodyCell} style={{color : '#fb6973'}}  align="left">{row[3] }</TableCell> }
              { row[3] === "Open" &&
              <TableCell className={classes.tableBodyCell} style={{color : '#71f494'}}   align="left">{row[3] }</TableCell> }

              <TableCell className={classes.tableBodyCell}  align="left">{row[4] }</TableCell> 
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <div className={classes.detailBoxWrapper}>
            <div className={classes.detailBox}>
                    <div className={classes.detailBoxHeading}>
                        Allocate kitchen zip code
                        <span style={{float: 'right', color : '#7CDE60'}}>
                        x</span>
                    </div>
                    <div className={classes.details}>
                        IL - Cook - Skokie - 60076 K1/22Miles
                        <span style={{float: 'right', color : '#92918F', fontSize:'12px'}}>
                        Remove</span>
                    </div>

                    <div className={classes.detailBoxHeading}>
                        &nbsp;
                    </div>
                    <div className={classes.detailBoxHeading}>
                        Admin Zip Code status &nbsp; &nbsp; 
                        {/*
                        <Button variant="contained" style={{border: '1px solid #707070', color : '#3B3A37'}}  className={classes.statusButton}>
                            Close
                        </Button>
                        <Button variant="contained" style={{border: '1px solid #0BBE0B', color : '#0BBE0B'}}  color="primary" className={classes.statusButton}>
                            Open
                        </Button> */}
                        <Switch
                          value="checkedA"
                          color="primary"
                        />
                    </div>
                    <div className={classes.detailBoxHeading}>
                        &nbsp;
                    </div>
                    
                    <div className={classes.detailBoxHeading}>
                    + Allocate serving Zip code to Kitchen
                    </div>
                    <div className={classes.detailBoxHeading}>
                        &nbsp;
                    </div>
                    <div className={classes.detailBoxHeading}>
                        {dropDown}
                    </div>
                    <div className={classes.detailBoxHeading}>
                        &nbsp;
                    </div>

                    <div className={classes.detailsButton}>
                        <Button variant="contained" className={classes.cancelButton}>
                            Cancel
                        </Button>
                        &nbsp;&nbsp;
                        <Button variant="contained" className={classes.saveButton} >
                            Save
                        </Button>
                    </div>
            </div>
      </div>
    </Paper>
      </div> )
  };
}

ServingPackagesOperation.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ServingPackagesOperation);
