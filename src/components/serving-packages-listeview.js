import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';
import InputBase from '@material-ui/core/InputBase';
import Radio from '@material-ui/core/Radio';
import AddIcon from '@material-ui/icons/Add';
import {styles, CustomTableCell} from '../styles/serving-packages-listview'

let id = 0;
function createData(name, status, action) {
  id += 1;
  return { id, name, status, action };
}

const rows = [
  createData('K1', "Active" , "Edit" ),
  createData('K2', "Active" , "Edit" )
];


function ServingPackagesListView(props) {
  const { classes } = props;

  const BootstrapInput = withStyles(theme => ({
    root: {
      'label + &': {
        marginTop: theme.spacing.unit * 3,
      }
    },
    input: {
      height : 30,
      color : '#707071',
      borderRadius: 3,
      position: 'relative',
      backgroundColor: theme.palette.background.paper,
      border: '1px solid rgba(112, 112, 112, 0.2)',
      fontSize: 20,
      //padding: '10px 26px 10px 12px',
      padding: '5px 20px',
      width : '155px',
      //padding: '5px 18px',
      transition: theme.transitions.create(['border-color', 'box-shadow']),
      // Use the system font instead of the default Roboto font.
      fontFamily: [
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
      '&:focus': {
        borderRadius: 4,
        borderColor: '#80bdff',
        boxShadow: '0px 3px 6px rgba(0, 0, 0, 0.16)',
      },
    },
  }))(InputBase);

  const dropDown =  <FormControl >  
                    <NativeSelect value=""   input={<BootstrapInput name="none1s" id="non1s-customized-native-simple"  />}>
                        <option value="">Refine By Kitchen</option>
                    </NativeSelect>
                </FormControl>
      
  
  const bottomTbl_Header = <div className={classes.bottomTbl_Header}> 
                                <div className={classes.leftHeader}>
                                    Package Price & Exception
                                </div>
                                <div className={classes.rightHeader}>
                                    Package Status & Combo Setting
                                </div>
                          </div>

  const bottomTbl_1 = <Table className={classes.table2}> 
                            <TableRow className={classes.header}>
                                <CustomTableCell className={classes.table2Header}>&nbsp;</CustomTableCell>
                                <CustomTableCell align="left" className={classes.table2Header} >3 D = Total</CustomTableCell>
                                <CustomTableCell align="left" className={classes.table2Header} >4 D = Total</CustomTableCell>
                                <CustomTableCell align="left" className={classes.table2Header} >6 D = Total</CustomTableCell>
                                <CustomTableCell align="left" className={classes.table2Header} >Subscribers</CustomTableCell>
                            </TableRow>

                            <TableRow>
                                
                                <CustomTableCell align="left" className={classes.person_cell}>Person 1</CustomTableCell>
                                <CustomTableCell align="left" className={classes.table2Cells}>$11 = $33</CustomTableCell>
                                <CustomTableCell align="left" className={classes.table2Cells}>$12 = $48</CustomTableCell>
                                <CustomTableCell align="left" className={classes.table2Cells}>$13 = $78</CustomTableCell>
                                <CustomTableCell align="left" className={classes.total_cell}>1090</CustomTableCell>
                            </TableRow>

                            <TableRow>
                                
                                <CustomTableCell align="left" className={classes.person_cell}>Person 1</CustomTableCell>
                                <CustomTableCell align="left" className={classes.table2Cells}>$11 = $33</CustomTableCell>
                                <CustomTableCell align="left" className={classes.table2Cells}>$12 = $48</CustomTableCell>
                                <CustomTableCell align="left" className={classes.table2Cells}>$13 = $78</CustomTableCell>
                                <CustomTableCell align="left" className={classes.total_cell}>1090</CustomTableCell>
                            </TableRow>

                            <TableRow>
                                
                                <CustomTableCell align="left" className={classes.person_cell}>Person 1</CustomTableCell>
                                <CustomTableCell align="left" className={classes.table2Cells}>$11 = $33</CustomTableCell>
                                <CustomTableCell align="left" className={classes.table2Cells}>$12 = $48</CustomTableCell>
                                <CustomTableCell align="left" className={classes.table2Cells}>$13 = $78</CustomTableCell>
                                <CustomTableCell align="left" className={classes.total_cell}>1090</CustomTableCell>
                            </TableRow>
                        </Table>
      
      const dropDown1 = "None 1/S";
      const dropDown2 = "None 1/S 2D";
      const dropDown3 = "None / 4S / 2S+1D / 2D";

      const bottomTbl_2 = <Table className={classes.table2}> 
                              <TableHead className={classes.header}>
                                  <CustomTableCell className={classes.table2Header}>&nbsp;</CustomTableCell>
                                  <CustomTableCell align="left" className={classes.table2Header}>1P</CustomTableCell>
                                  <CustomTableCell align="left" className={classes.table2Header}>2P</CustomTableCell>
                                  <CustomTableCell align="left" className={classes.table2Header}>4P</CustomTableCell>
                              </TableHead>

                              <TableRow className={classes.header}>
                                <CustomTableCell className={classes.table2Header}>Main Cs</CustomTableCell>
                                  <CustomTableCell align="left" className={classes.person_cell}>
                                      {dropDown1}
                                  </CustomTableCell>
                                  <CustomTableCell align="left" className={classes.person_cell}>
                                      {dropDown1}
                                  </CustomTableCell>
                                  <CustomTableCell align="left" className={classes.person_cell}>
                                      {dropDown1}
                                </CustomTableCell>
                                  
                              </TableRow>

                              <TableRow className={classes.header}>
                                <CustomTableCell className={classes.table2Header}>Sides</CustomTableCell>
                                  <CustomTableCell align="left" className={classes.person_cell}>
                                      {dropDown2}
                                  </CustomTableCell>
                                  <CustomTableCell align="left" className={classes.person_cell}>
                                      {dropDown2}
                                  </CustomTableCell>
                                  <CustomTableCell align="left" className={classes.person_cell}>
                                      {dropDown2}
                                </CustomTableCell>
                                  
                              </TableRow>
                              <TableRow className={classes.header}>
                                <CustomTableCell className={classes.table2Header}>DD</CustomTableCell>
                                  <CustomTableCell align="left" className={classes.person_cell}>
                                      {dropDown3}
                                  </CustomTableCell>
                                  <CustomTableCell align="left" className={classes.person_cell}>
                                      {dropDown3}
                                  </CustomTableCell>
                                  <CustomTableCell align="left" className={classes.person_cell}>
                                      {dropDown3}
                                  </CustomTableCell>
                              </TableRow>
                          </Table>
          
          const exceptionTblHeader = <div className={classes.blueLabel}>Exception</div>
          const pckStsTblHeader = <div className={classes.blueLabel}>Package Status</div>

          const excptionsTbl = <Table className={classes.table3}> 
                                  <TableRow className={classes.header}>
                                      <CustomTableCell className={classes.table2Header}>60645</CustomTableCell>
                                      <CustomTableCell align="left" className={classes.table2Cells}>+2.5%</CustomTableCell>
                                  </TableRow>
                                  <TableRow className={classes.header}>
                                      <CustomTableCell className={classes.table2Header}>60645</CustomTableCell>
                                      <CustomTableCell align="left" className={classes.table2Cells}>+2.5%</CustomTableCell>
                                  </TableRow>
                                </Table>
          
          const packageStstTbl = <Table className={classes.table3}> 
                                    <TableHead className={classes.header}>
                                        <CustomTableCell className={classes.table2Header}>&nbsp;</CustomTableCell>
                                        <CustomTableCell align="left" className={classes.table2Header}>1P</CustomTableCell>
                                        <CustomTableCell align="left" className={classes.table2Header}>2P</CustomTableCell>
                                        <CustomTableCell align="left" className={classes.table2Header}>4P</CustomTableCell>
                                    </TableHead>

                                    <TableRow className={classes.header}>
                                      <CustomTableCell className={classes.table2Header}>1 Person</CustomTableCell>
                                        <CustomTableCell align="left" className={classes.person_cell}>
                                            Yes
                                        </CustomTableCell>
                                        <CustomTableCell align="left" className={classes.person_cell}>
                                            No
                                        </CustomTableCell>
                                        <CustomTableCell align="left" className={classes.person_cell}>
                                        Yes
                                      </CustomTableCell>
                                    </TableRow>

                                    <TableRow className={classes.header}>
                                      <CustomTableCell className={classes.table2Header}>2 Person</CustomTableCell>
                                        <CustomTableCell align="left" className={classes.person_cell}>
                                        Yes
                                        </CustomTableCell>
                                        <CustomTableCell align="left" className={classes.person_cell}>
                                            No
                                        </CustomTableCell>
                                        <CustomTableCell align="left" className={classes.person_cell}>
                                        Yes
                                      </CustomTableCell>
                                    </TableRow>
                                  </Table>
  return (
    <div style={{padding : '1rem 2rem'}}>
        <div className={classes.filterBox}>
            <span className={classes.heading}>
              Serving Packages
            </span>
            {/*
            <Button variant="contained" className={classes.button}>
                Refine By Kitchen
            </Button> */}
            {dropDown}
            
            <Button variant="contained" style={{float : 'right'}} className={classes.button}>
              Add Product &nbsp;<AddIcon style={{float: 'right'}}/>
            </Button>
        </div>
     <Paper className={classes.root}>
    
      
      <Table className={classes.table}>
        <TableHead >
    
          <TableRow style={{border : '1px solid #d0c8c8'}} className={classes.tableHeight}>
            <TableCell className={classes.tableHead}>&nbsp;</TableCell>
            <TableCell align="left" className={classes.tableHead}>Kitchen Name</TableCell>
            <TableCell align="left" className={classes.tableHead}>Kitchen Status</TableCell>
            <TableCell align="left" className={classes.tableHead}>Action</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map(row => (
            <TableRow key={row.id} className={classes.tableHeight}>
              <TableCell align="left" component="th" scope="row" className={classes.noBorder}>
                {row.id === 1 &&
                <Radio checked={true} value="d" color="default" name="radio-button-demo" aria-label="D" /> }
                
                {row.id !==1 &&
                <Radio checked={false} value="d" color="default" name="radio-button-demo" aria-label="D" /> }      
              </TableCell>
              <TableCell align="left" component="th" scope="row" className={classes.noBorder}>
                {row.name}
              </TableCell>
              <TableCell align="left" className={classes.noBorder}>{row.status}</TableCell>
              <TableCell align="left" className={classes.noBorder}>
                <Button className={classes.button}>Edit</Button>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
       <div className={classes.bottomTblDiv}>
            {bottomTbl_Header}

            <div  style={{width:'45%', float: 'left', padding: '1rem 1.5rem'}}>
                {bottomTbl_1}
                {exceptionTblHeader}
                {excptionsTbl}
            </div>
            
            <div  style={{width:'55%', float: 'left', padding: '1rem 1.5rem', borderLeft : '1px solid #ababab' }}>
                 {bottomTbl_2}
                 {pckStsTblHeader}
                 {packageStstTbl}
            </div>
       </div>
    </Paper>
    </div>
  );
}

ServingPackagesListView.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ServingPackagesListView);
