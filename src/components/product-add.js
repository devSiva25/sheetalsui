import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';
import TextField from '@material-ui/core/TextField';
import Switch from '@material-ui/core/Switch';
import {styles, BootstrapInput} from './../styles/product-add';


function ProductAdd(props) {
  const { classes } = props;
  
  const dropDown =  <FormControl >  
                        <NativeSelect value=""   input={<BootstrapInput name="none1s" id="non1s-customized-native-simple"  />}>
                            <option value="">Select Reason</option>
                        </NativeSelect>
                    </FormControl>
  
  return (
    <div className={classes.contentBoxWrapper}>


        
            <div className={classes.contentBox}>
                <div className={classes.headerBox}>

                    <span style={{color: '#2C90F3', fontWeight: 600, display : 'inline-block', 'marginRight' : '70px' }}>
                        Product Detail 
                    </span>

                    <span style={{color: '#000', fontWeight: 600, display : 'inline-block', 'marginRight' : '70px' }}>
                        visibility by Days & Kitchen | Pay as you go
                    </span>

                    <Button variant="contained" style={{float : 'right'}} className={classes.button}>
                    + Add Product
                    </Button>
                </div>
                <div className={classes.tableBox}>
                    <Table className={classes.table2}>
                        <TableBody >
                            <TableRow className={classes.tableHeight2}>
                                <TableCell align="left" className={classes.detail_cell} >Product Id :</TableCell>
                                <TableCell align="left" className={classes.detail_cell}>447786</TableCell>
                            </TableRow>
                            <TableRow className={classes.tableHeight2}>
                                <TableCell align="left" className={classes.detail_cell} >Recipe Name :</TableCell>
                                <TableCell align="left" className={classes.table2Cells} style={{color: '#B9B9B9'}}>
                                    <TextField id="recipe-name" className={classes.textField} defaultValue="" margin="normal" variant="outlined" InputProps={{style : {height : '35px'} }} />
                                </TableCell>
                            </TableRow>

                            <TableRow className={classes.tableHeight2}>
                                <TableCell align="left" className={classes.detail_cell} >Recipe With :</TableCell>
                                <TableCell align="left" className={classes.table2Cells} style={{color: '#B9B9B9'}}>
                                    <TextField id="recipe-with" className={classes.textField} defaultValue="" margin="normal" variant="outlined" InputProps={{style : {height : '35px'} }} />
                                </TableCell>
                            </TableRow>

                            <TableRow className={classes.tableHeight2}>
                                <TableCell align="left" className={classes.detail_cell} >Menu Type :</TableCell>
                                <TableCell align="left" className={classes.table2Cells} style={{color: '#B9B9B9'}}>
                                    {dropDown}
                                </TableCell>
                            </TableRow>

                            <TableRow className={classes.tableHeight2}>
                                <TableCell align="left" className={classes.detail_cell} >Ingredient :</TableCell>
                                <TableCell align="left" className={classes.table2Cells} style={{color: '#B9B9B9'}}>
                                    <TextField id="outlined-multiline-flexible" label="" multiline rowsMax="8" value=""   className={classes.textField}  margin="normal"   variant="outlined"  />
                                </TableCell>
                            </TableRow>

                            <TableRow className={classes.tableHeight2}>
                                <TableCell align="left" className={classes.detail_cell} >Food Type :</TableCell>
                                <TableCell align="left" className={classes.table2Cells} style={{color: '#B9B9B9'}}>
                                    {dropDown}
                                </TableCell>
                            </TableRow>

                            <TableRow className={classes.tableHeight2}>
                                <TableCell align="left" className={classes.detail_cell} >Product Image :</TableCell>
                                <TableCell align="left" className={classes.table2Cells} style={{color: '#B9B9B9'}}>
                                    <input type="file" />
                                </TableCell>
                            </TableRow>

                            <TableRow className={classes.tableHeight2}>
                                <TableCell align="left" className={classes.detail_cell} >Serving Quality Type :</TableCell>
                                <TableCell align="left" className={classes.table2Cells} style={{color: '#B9B9B9'}}>
                                    <input type="file" />
                                </TableCell>
                            </TableRow>

                            <TableRow className={classes.tableHeight2}>
                                <TableCell align="left" className={classes.detail_cell} >Ask for Allergic ingredient</TableCell>
                                <TableCell align="left" className={classes.table2Cells} style={{color: '#B9B9B9'}}>
                                    <Switch checked='true'  value="true"  />   
                                </TableCell>
                            </TableRow>

                        </TableBody>
                    </Table>
                    <div className={classes.buttonBox}>
                        <Button variant="contained" className={classes.cancelBtn}>
                            Cancel
                        </Button>
                        <Button variant="contained" className={classes.login}>
                            Go to product visibility
                        </Button>
                    </div>
                </div>
            </div>
    </div>
  );
}

ProductAdd.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProductAdd);
