import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import {styles, CustomTableCell} from './../styles/package-status';
  
  function PackageStatus(props){
    const {classes} = props;

     const state = {
        on : true
     }

    const switchBtn = <FormControlLabel
                        control={
                          <Switch
                            classes={{
                              switchBase: classes.iOSSwitchBase,
                              bar: classes.iOSBar,
                              icon: classes.iOSIcon,
                              iconChecked: classes.iOSIconChecked,
                              checked: classes.iOSChecked,
                            }}
                            disableRipple
                            checked={true}
                            
                            value="checkedB"
                          />
                        }
                        label=""
                      />

    

    return(
        <div>
            <Table className={classes.table}> 
                <TableHead className={classes.header}>

                    <CustomTableCell align="left" className={classes.header_padding}>&nbsp;</CustomTableCell>
                    <CustomTableCell align="left" className={classes.header_padding}>3 Days</CustomTableCell>
                    <CustomTableCell align="left" className={classes.header_padding}>4 Days</CustomTableCell>
                    <CustomTableCell align="left" className={classes.header_padding}>6 Days</CustomTableCell>
                </TableHead>

                <TableRow className={classes.header}>
                    <CustomTableCell align="left" className={classes.header_padding}>
                        Person 1
                    </CustomTableCell>
                    <CustomTableCell align="left" className={classes.header_padding}>
                        {switchBtn}
                    </CustomTableCell>
                    <CustomTableCell align="left" className={classes.header_padding}>
                        {switchBtn}
                    </CustomTableCell>
                    <CustomTableCell align="left" className={classes.header_padding}>
                        {switchBtn}
                   </CustomTableCell>
                </TableRow>

                <TableRow className={classes.header}>
                    <CustomTableCell align="left" className={classes.header_padding}>
                        Person 1
                    </CustomTableCell>
                    <CustomTableCell align="left" className={classes.header_padding}>
                        {switchBtn}
                    </CustomTableCell>
                    <CustomTableCell align="left" className={classes.header_padding}>
                        {switchBtn}
                    </CustomTableCell>
                    <CustomTableCell align="left" className={classes.header_padding}>
                        {switchBtn}
                   </CustomTableCell>
                </TableRow>

                <TableRow className={classes.header}>
                    <CustomTableCell align="left" className={classes.header_padding}>
                        Person 1
                    </CustomTableCell>
                    <CustomTableCell align="left" className={classes.header_padding}>
                        {switchBtn}
                    </CustomTableCell>
                    <CustomTableCell align="left" className={classes.header_padding}>
                        {switchBtn}
                    </CustomTableCell>
                    <CustomTableCell align="left" className={classes.header_padding}>
                        {switchBtn}
                   </CustomTableCell>
                </TableRow>


            </Table>
            <div> &nbsp; </div>
            {/*buttonFooter */}
        </div>        
    )
}  


PackageStatus.prototype = {
    classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(PackageStatus);