import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';
import {styles, BootstrapInput } from './../styles/kitchen-list-view';

let id = 0;
function createData(name, city, state, status, zipcodes, orderscap, drivers, subscribers, products, lastwkorder) {
  id += 1;
  return { name, city, state, status, zipcodes, orderscap, drivers, subscribers, products, lastwkorder };
}

const rows = [
  createData('K1', "Chicago" , "IL", "Active", 70, 2000, 50, 5367, 70, 1010 ),
];


function KitchenListView(props) {
  const { classes } = props;
  
  const refinKitchenDD =  <FormControl >  
                            <NativeSelect value=""   input={<BootstrapInput name="none1s" id="non1s-customized-native-simple1"  />}>
                                <option value="">Refine By Kitchen</option>
                            </NativeSelect>
                          </FormControl>

  const refineStatusDD =  <FormControl >  
                              <NativeSelect value=""   input={<BootstrapInput name="none1s" id="non1s-customized-native-simple2"  />}>
                                  <option value="">Refine By Status</option>
                              </NativeSelect>
                            </FormControl>
  
  return (
    <div style={{padding : '2rem'}}>
        <div>
        
            <span className={classes.title}>Kitchens</span>
            {/*
            <Button variant="contained" className={classes.button}>
                Refine By Kitchen
            </Button>*/}
            {refinKitchenDD}&nbsp;&nbsp;
            {/*
            <Button variant="contained" className={classes.button}>
                Refine By Status
            </Button>*/}
            {refineStatusDD}&nbsp;&nbsp;
            <Button variant="contained" style={{float : 'right'}} className={classes.button}>
            + Add Product
            </Button>
        </div>
     <Paper className={classes.root}>
    
      
      <Table className={classes.table}>
        <TableHead >
    
          <TableRow style={{border : '1px solid #d0c8c8'}} className={classes.tableHeight}>
            <TableCell className={classes.tableHead}>Kitchen Name</TableCell>
            <TableCell align="left" className={classes.tableHead}>City</TableCell>
            <TableCell align="left" className={classes.tableHead}>State</TableCell>
            <TableCell align="left" className={classes.tableHead}>Status</TableCell>
            <TableCell align="left" className={classes.tableHead}># Serving Zip Codes</TableCell>
            <TableCell align="left" className={classes.tableHead}># Order Capacity</TableCell>
            <TableCell align="left" className={classes.tableHead}># Drivers</TableCell>
            <TableCell align="left" className={classes.tableHead}># Subscribers</TableCell>
            <TableCell align="left" className={classes.tableHead}># Products</TableCell>
            <TableCell align="left" className={classes.tableHead}># Last Week Orders</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map(row => (
            <TableRow key={row.id} className={classes.tableHeight}>
             <TableCell align="left" component="th" scope="row" className={classes.noBorder}>
                {row.name}
              </TableCell>
              <TableCell align="left" component="th" scope="row" className={classes.noBorder}>
                {row.city}
              </TableCell>
              <TableCell align="left" className={classes.noBorder}>{row.state}</TableCell>
              <TableCell align="left" className={classes.noBorder}>{row.status}</TableCell>
              <TableCell align="left" component="th" scope="row" className={classes.noBorder}>
                {row.zipcodes}
              </TableCell>
              <TableCell align="left" className={classes.noBorder}>{row.orderscap}</TableCell>
              <TableCell align="left" className={classes.noBorder}>{row.drivers}</TableCell>
              <TableCell align="left" className={classes.noBorder}>{row.subscribers}</TableCell>
              <TableCell align="left" className={classes.noBorder}>{row.products}</TableCell>
              <TableCell align="left" className={classes.noBorder}>{row.lastwkorder}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
    </div>
  );
}

KitchenListView.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(KitchenListView);
