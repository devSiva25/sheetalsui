import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import {styles,CustomTableCell, BootstrapInput} from './../styles/package-pricing';


class PackagePricing extends React.Component{
    
    addException = false;
    state = {
      showExceptionForm : this.addException
    }

    toggleException = () => {
      this.setState( state => ({showExceptionForm : !state.showExceptionForm}));
    }

    render(){
      const { classes } = this.props;
      
        return(
                <div>
                <Table className={classes.table}> 
                    <TableRow className={classes.header}>
                        <CustomTableCell className={classes.header_padding}>&nbsp;</CustomTableCell>
                        <CustomTableCell align="left" className={classes.header_padding}>3 Days</CustomTableCell>
                        <CustomTableCell align="left" className={classes.header_padding}>Total</CustomTableCell>
                        <CustomTableCell align="left" className={classes.header_padding}>4 Days</CustomTableCell>
                        <CustomTableCell align="left" className={classes.header_padding}>Total</CustomTableCell>
                        <CustomTableCell align="left" className={classes.header_padding}>6 Days</CustomTableCell>
                        <CustomTableCell align="left" className={classes.header_padding}>Total</CustomTableCell>
                    </TableRow>

                    <TableRow className={classes.header}>
                        
                        <CustomTableCell align="left" className={classes.header_padding}>Person 1</CustomTableCell>
                        <CustomTableCell align="left" className={classes.header_padding}>
                          <TextField className={classes.textField} defaultValue="$11" margin="normal" variant="outlined" InputProps = {{
                            classes : {input : classes.textbox} }} />
                        </CustomTableCell>
                        <CustomTableCell align="left" className={classes.total_cell}>$33</CustomTableCell>
                        <CustomTableCell align="left" className={classes.header_padding}>
                          <TextField className={classes.textField} defaultValue="$12" margin="normal" variant="outlined" InputProps = {{
                            classes : {input : classes.textbox} }} />
                        </CustomTableCell>
                        <CustomTableCell align="left" className={classes.total_cell}>$48</CustomTableCell>
                        <CustomTableCell align="left" className={classes.header_padding}>
                          <TextField className={classes.textField} defaultValue="$13" margin="normal" variant="outlined" InputProps = {{
                            classes : {input : classes.textbox} }} />
                        </CustomTableCell>
                        <CustomTableCell align="left" className={classes.total_cell}>$78</CustomTableCell>
                    </TableRow>

                    <TableRow className={classes.header}>
                        
                        <CustomTableCell align="left" className={classes.header_padding}>Person 2</CustomTableCell>
                        <CustomTableCell align="left" className={classes.header_padding}>
                            <TextField className={classes.textField} defaultValue="$11" margin="normal" variant="outlined" InputProps = {{
                            classes : {input : classes.textbox} }} />
                        </CustomTableCell>
                        <CustomTableCell align="left" className={classes.total_cell}>$33</CustomTableCell>
                        <CustomTableCell align="left" className={classes.header_padding}>
                           <TextField className={classes.textField} defaultValue="$12" margin="normal" variant="outlined" InputProps = {{
                            classes : {input : classes.textbox} }} />
                        </CustomTableCell>
                        <CustomTableCell align="left" className={classes.total_cell}>$48</CustomTableCell>
                        <CustomTableCell align="left" className={classes.header_padding}>
                           <TextField className={classes.textField} defaultValue="$13" margin="normal" variant="outlined" InputProps = {{
                            classes : {input : classes.textbox} }} />
                        </CustomTableCell>
                        <CustomTableCell align="left" className={classes.total_cell}>$78</CustomTableCell>
                    </TableRow>

                    <TableRow className={classes.header}>
                        
                        <CustomTableCell align="left" className={classes.header_padding}>Person 3</CustomTableCell>
                        <CustomTableCell align="left" className={classes.header_padding}>
                           <TextField className={classes.textField} defaultValue="$11" margin="normal" variant="outlined" InputProps = {{
                            classes : {input : classes.textbox} }} />
                        </CustomTableCell>
                        <CustomTableCell align="left" className={classes.total_cell}>$33</CustomTableCell>
                        <CustomTableCell align="left" className={classes.header_padding}>
                          <TextField className={classes.textField} defaultValue="$12" margin="normal" variant="outlined" InputProps = {{
                            classes : {input : classes.textbox} }} />
                        </CustomTableCell>
                        <CustomTableCell align="left" className={classes.total_cell}>$48</CustomTableCell>
                        <CustomTableCell align="left" className={classes.header_padding}>
                          <TextField className={classes.textField} defaultValue="$13" margin="normal" variant="outlined" InputProps = {{
                            classes : {input : classes.textbox} }} />
                        </CustomTableCell>
                        <CustomTableCell align="left" className={classes.total_cell}>$78</CustomTableCell>
                    </TableRow>
                </Table>
                
                <div>
                    <div className={classes.leftBox}>
                      <div className={classes.add_exception_label} onClick={this.toggleException} >
                        + Exception
                      </div>

                      { this.state.showExceptionForm &&
                        <div style={{marginBottom : '1rem'}}>
                          <form className={classes.root} autoComplete="off">
                              <FormControl className={classes.margin}>  
                              <NativeSelect value="" input={<BootstrapInput name="age" id="age-customized-native-simple" />}>
                                    <option value="">Select Zip Code</option>
                                    <option value={10}>Ten</option>
                                    <option value={20}>Twenty</option>
                                    <option value={30}>Thirty</option>
                                  </NativeSelect>
                                </FormControl>
                                &nbsp;&nbsp;
                                <FormControl className={classes.margin}>
                                  <NativeSelect value="" input={<BootstrapInput name="age" id="age-customized-native-simple" />}>
                                    <option value="">%</option>
                                    <option value={10}>Ten</option>
                                    <option value={20}>Twenty</option>
                                    <option value={30}>Thirty</option>
                                  </NativeSelect>
                                </FormControl>
                                &nbsp;&nbsp;
                                <Button variant="contained"  className={classes.button + classes.margin } >
                                  Save
                                </Button>
                              </form>
                          </div>
                        }

                        <div>
                        <Table className={classes.table2}> 
                              <TableRow className={classes.header}> 
                                  <CustomTableCell align="left" className={classes.header_padding} style={{border: 'none'}}>
                                    <input type="checkbox" value="1"/>
                                  </CustomTableCell>
                                  <CustomTableCell align="left" className={classes.header_padding} style={{border: 'none'}}>60645</CustomTableCell>
                                  <CustomTableCell align="left" className={classes.header_padding} style={{border: 'none'}}>+2.5%</CustomTableCell>
                                  <CustomTableCell align="left" className={classes.total_cell} style={{border: 'none'}}>Remove</CustomTableCell>
                              </TableRow>
                              <TableRow className={classes.header}> 
                                  <CustomTableCell align="left" className={classes.header_padding} style={{border: 'none'}}>
                                    <input type="checkbox" value="1"/>
                                  </CustomTableCell>
                                  <TableCell align="left" className={classes.header_padding} style={{border: 'none'}}>60645</TableCell>
                                  <CustomTableCell align="left" className={classes.header_padding} style={{border: 'none'}}>+2.5%</CustomTableCell>
                                  <CustomTableCell align="left" className={classes.total_cell} style={{border: 'none'}}>Remove</CustomTableCell>
                              </TableRow>
                              <TableRow className={classes.header}> 
                                  <CustomTableCell align="left" className={classes.header_padding} style={{border: 'none'}}>
                                    <input type="checkbox" value="1"/>
                                  </CustomTableCell>
                                  <CustomTableCell align="left" className={classes.header_padding} style={{border: 'none'}}>60645</CustomTableCell>
                                  <CustomTableCell align="left" className={classes.header_padding} style={{border: 'none'}}>+2.5%</CustomTableCell>
                                  <CustomTableCell align="left" className={classes.total_cell} style={{border: 'none'}}>Remove</CustomTableCell>
                              </TableRow>
                        </Table>
                        </div>
                    </div>
                </div>
                <div className={classes.leftBox} style={{paddingLeft: '4rem'}}>
                      <div>
                        Exception 60645 (2.5%)
                      </div>
                      <div>
                      <Table className={classes.table2} style={{marginTop : '20px', border : '1px solid #e0e0e0'}}>
                        <TableHead>
                          <TableRow className={classes.header}>
                            <TableCell align="left" className={classes.header_padding}>3 Days</TableCell>
                            <TableCell align="left" className={classes.header_padding}>4 Days</TableCell>
                            <TableCell align="left" className={classes.header_padding}>6 Days</TableCell>
                          </TableRow>
                        </TableHead>
                        <TableBody>
                            <TableRow key="1" className={classes.header}>
                              <TableCell component="th" scope="row" className={classes.header_padding}>$22</TableCell>
                              <TableCell align="left" className={classes.header_padding}>$22</TableCell>
                              <TableCell align="left" className={classes.header_padding}>$22</TableCell>
                            </TableRow>
                        </TableBody>
                        <TableBody>
                            <TableRow key="1" className={classes.header}>
                              <TableCell component="th" scope="row" className={classes.header_padding}>$22</TableCell>
                              <TableCell align="left" className={classes.header_padding}>$22</TableCell>
                              <TableCell align="left" className={classes.header_padding}>$22</TableCell>
                            </TableRow>
                        </TableBody>
                      </Table>

                      </div>
                  </div>
                  {/*buttonFooter*/}
                </div>
        )
    }
}

PackagePricing.propTypes = {
    classes: PropTypes.object.isRequired,
};
export default withStyles(styles, { withTheme: true })(PackagePricing);