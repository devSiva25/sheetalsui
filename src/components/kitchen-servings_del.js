import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import SwipeableViews from 'react-swipeable-views';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import ServingPAckages from './package-pricing';
import ComboSettings from './combo-settings';
import ServingPackages0 from './package-status';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';
import InputBase from '@material-ui/core/InputBase';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import AddIcon from '@material-ui/icons/Add';


function TabContainer({ children, dir }) {
    return (
      <Typography component="div" dir={dir} style={{ padding: 8 * 3 }}>
        {children}
      </Typography>
    );
  }
  
  TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
    dir: PropTypes.string.isRequired,
  };
  
  const styles = theme => ({
    root: {
      backgroundColor: theme.palette.background.paper,
      width: '100%',
      padding : '2rem'
    },
    filterBox :{
      marginBottom : '2rem'
    },
    button : {
      background : '#FFF'
    },
    formControl: {
      margin: theme.spacing.unit,
      minWidth: 120,
    },
    selectEmpty: {
      marginTop: theme.spacing.unit * 2,
    },
    rightIcon : {
      float : 'right'
    }
  });

  const BootstrapInput = withStyles(theme => ({
    root: {
      'label + &': {
        marginTop: theme.spacing.unit * 3,
      }
    },
    input: {
      height : 30,
      color : '#707071',
      borderRadius: 3,
      position: 'relative',
      backgroundColor: theme.palette.background.paper,
      border: '1px solid rgba(112, 112, 112, 0.2)',
      fontSize: 20,
      //padding: '10px 26px 10px 12px',
      padding: '5px 20px',
      width : '155px',
      //padding: '5px 18px',
      transition: theme.transitions.create(['border-color', 'box-shadow']),
      // Use the system font instead of the default Roboto font.
      fontFamily: [
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
      '&:focus': {
        borderRadius: 4,
        borderColor: '#80bdff',
        boxShadow: '0px 3px 6px rgba(0, 0, 0, 0.16)',
      },
    },
  }))(InputBase);
      
  
  class ServingsTable extends React.Component {
    state = {
      value: 0,
      labelWidth: 0
    };
  
    handleChange = (event, value) => {
      this.setState({ value });
    };
  
    handleChangeIndex = index => {
      this.setState({ value: index });
    };


  
    render() {
      const { classes, theme } = this.props;
      
      const dropDown =  <FormControl >  
                          <NativeSelect value=""   input={<BootstrapInput name="none1s" id="non1s-customized-native-simple"  />}>
                              <option value="">Select Kitchen</option>
                          </NativeSelect>
                      </FormControl>

      const dropDown1 = <FormControl variant="outlined" className={classes.formControl}>
                          <InputLabel ref={ref => {this.InputLabelRef = ref;}} htmlFor="outlined-age-native-simple">
                            Select Kitchen
                          </InputLabel>
                          <Select native value="Select Kitchen" 
                            input={
                              <OutlinedInput name="age" labelWidth={this.state.labelWidth} id="outlined-age-native-simple"/> }>
                            <option value="Select Kitchen" />
                            <option value={10}>Ten</option>
                          </Select>
                        </FormControl>
      
      return (
        <div className={classes.root}>
          <div className={classes.filterBox}>
            
            {/*
            <Button variant="contained" className={classes.button}>
                Refine By Kitchen
            </Button> */}

            {dropDown}
            
            <Button variant="contained" style={{float: 'right'}} className={classes.button}>
                Add Product &nbsp;
                <AddIcon className={classes.rightIcon} />
            </Button>
        </div>
          <AppBar position="static" color="default">
            <Tabs
              value={this.state.value}
              onChange={this.handleChange}
              indicatorColor="primary"
              textColor="primary"
            >
              <Tab label="Package Status" />
              <Tab label="Package Pricing" />
              <Tab label="Combo Setting" />
            </Tabs>
          </AppBar>
          <SwipeableViews
            axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
            index={this.state.value}
            onChangeIndex={this.handleChangeIndex}
          >
           
            <TabContainer dir={theme.direction}>
                  <ServingPackages0/>
            </TabContainer>
            <TabContainer dir={theme.direction}>
               <ServingPAckages/>
            </TabContainer>
            <TabContainer dir={theme.direction}>
              <ComboSettings/>
            </TabContainer>
          </SwipeableViews>
        </div>
      );
    }
  }
  
  ServingsTable.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
  };
  
  export default withStyles(styles, { withTheme: true })(ServingsTable);
  