
const styles = theme => ({
  root: {
    width: 'auto',
    //marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    color : '#000',
    width : 'auto'
  },
  button: {
    margin: theme.spacing.unit,
    backgroundColor : "#FFF"
  },
  tableHeadCell : {
      padding : '4px 20px 4px 24px !important',
      fontWeight : '600 !important',
      color : '#64A1F1 !important',
      border : 'none'
  },
  tableBodyCell : {
    padding : '4px 20px 4px 24px !important',
    color : '#000 !important',
    fontWeight : '600 !important',
    border : 'none'
}
});

export default styles;
