
import InputBase from '@material-ui/core/InputBase';
import { withStyles } from '@material-ui/core/styles';




export const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  contentBoxWrapper:  {padding : '0rem',background : '#FFF', float: 'left', width : '100%'},
  headerBox :  {marginBottom : 10, marginTop:10,padding : '17px 3rem', borderTop: '1px solid rgba(112, 112, 112, 0.5)', borderBottom: '1px solid rgba(112, 112, 112, 0.5)'},
  table: {
    minWidth: 700,
    color : '#000'
  },
  button: {
    //margin: theme.spacing.unit,
    backgroundColor : "#FFF",
    marginTop: -7
  },
  tableBox :{float:'left', 'width': '100%', padding: '1rem 3rem'},
  tableHead:{
      fontWeight : 500,
      color : '#000',
      border :'none',
      background: '#f8f8f8',
      borderRight : '1px solid #b1a6a6',
      textAlign : 'center'
  },
  headingLabel:{
    fontWeight : 600,
    color : '#000',
    marginBottom : '1.5rem'
 },
  noBorder : {
      border: 'none',
      
      padding: '0px 20px 0px 24px'
  },
  tableHeight : {
      height : '45px'
      
  },
  tableHeight2 : {
    height : '10px'  
  },
  bottomTblDiv : {
    marginTop : '3rem'
  },
  bottomTbl_Header:{
    padding : "6px 4px",
    background : "#f8f8f8",
    boxShadow : "0px 1px 5px 0px rgba(0,0,0,0.2), 0px 2px 2px 0px rgba(0,0,0,0.14), 0px 3px 1px -2px rgba(0,0,0,0.12)",
    float : 'left',
    width : '100%',
    fontWeight: 600,
    
  },
  leftHeader : {
    width : '50%',
    color : '#000',
    padding: '0 2rem',
    float : 'left'
  },
  rightHeader : {
    width : '50%',
    color : '#000',
    padding: '0 2rem',
    float : 'left'
  },
  
  table2 : {
      minWidth : 'auto',
      maxWidth : 500
  },
  table2Cells : {
    padding: '4px 6px',
    fontSize: '16px',
    border: 'none',
    color : '#7d7474'
  },
  table2Header : {
    padding: '10px',
    border : 'none',
    background: '#fff',
    color: '#000'
  },
  total_cell : {
    padding: '10px',
    fontSize: '16px',
    border: 'none',
    color : '#000'
  },
  detail_cell : {
    padding: '10px 6px',
    fontSize: '16px',
    border: 'none',
    color : '#000',
    fontWeight : 500
  },
  blueLabel : {
    color : '#468BF2',
    padding: '1rem 0.7rem'
  },
  table3 : {
    maxWidth : '200px'
  },
  table4 : {
    maxWidth : '300px'
  },
  contentBox : {
      padding : '0',
      width : '100%',
      float : 'left',
  },
  floatLeft : {
      width : '50%',
      float : 'left',
      minWidth :"250px"
  },
  rightLeft : {
    width : '50%',
    float : 'left',
    minWidth :"250px"
  },
  buttonBox : {
      width : '100%',
      float : 'left',
      textAlign : 'center',
      padding : '2rem 0'
  },
  login : {
      background : '#F6DD97',
      color : '#000000'
  },
  contentBox2 : {
    border: '1px solid #707070',
    
    width : '100%',
    float : 'left',
  },
  statusButton : {
      borderRadius : 0,
      background : '#FFF',
      color : '#000',
      border: 'none',
      boxShadow: 'none',
      padding : '2px 16px'
  },
  dropDown : {
    borderRadius: '0',
    padding: '8px 26px'
  },
  cancelBtn : {
      background : '#fff',
      color : '#373535',
      marginRight : '10px',
      border:  '1px solid #7A77E8'
  },
  saveBtn: {
      background : '#82EEAD',
      border: '1px solid #077712',
  },
  textField : {
    marginRight: theme.spacing.unit,
    width : '100%'
  },
  inputText : {
      height : '35px',
      width : '100%'
  }


});



export const BootstrapInput = withStyles(theme => ({
    root: {
      'label + &': {
        marginTop: theme.spacing.unit * 3,
      },
    },
    input: {
      borderRadius: 4,
      position: 'relative',
      backgroundColor: theme.palette.background.paper,
      border: '1px solid #ced4da',
      fontSize: 14,
      width: '120px',
      //padding: '10px 26px 10px 16px',
      padding: '5px 18px',
      transition: theme.transitions.create(['border-color', 'box-shadow']),
      // Use the system font instead of the default Roboto font.
      fontFamily: [
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
      '&:focus': {
        borderRadius: 4,
        borderColor: '#80bdff',
        boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
      },
    },
  }))(InputBase);
  
