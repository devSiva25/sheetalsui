
import InputBase from '@material-ui/core/InputBase';
import TableCell from '@material-ui/core/TableCell';
import { withStyles } from '@material-ui/core/styles';

export const CustomTableCell = withStyles(theme => ({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    }
  }))(TableCell);
  
  export const styles = theme => ({
    root: {
      width: '100%',
      marginTop: theme.spacing.unit * 3,
      overflowX: 'auto',
    },
    button: {
      margin: theme.spacing.unit,
    },
    table: {
      maxWidth: 700,
    },
    row: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.background.default,
      },
    },
    header : {
      fontSize:12,
      color : '#333',
      fontWeight : 600,
      height: 'auto',
    },
    header_padding:
    {
      padding: '10px 18px 10px 16px',
      border : 'none',
      background : '#FFF',
      color : '#000'
    },
    total_cell : {
      padding: '4px 18px 4px 16px',
      color : '#5682FC'
    },
    leftBox :
    {
       float: "left",
       width : "50%",
       padding: "2.5rem 5px 1rem"
    },
    add_exception_label :{
      color : "#5682FC"
    },
    margin  :{
      margin : '0 10px'
    },
    table_row : {
      height : 'auto'
    },
    cancelButton :{
       margin : theme.spacing.unit,
       color : '#000'
    },
    button_footer : {
       textAlign : 'center',
       padding : '10px 0',
       width : '100%',
       float : 'left'
    },
    saveButton : {
      background : '#F6DD97',
      color : '#000000'
    },
    dropDownPadding : {
        padding: '5px 18px'
    },
    chips: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    chip: {
      margin: theme.spacing.unit / 4,
    },
    noLabel: {
      marginTop: theme.spacing.unit * 3,
    },
  });
  
  export const BootstrapInput = withStyles(theme => ({
    root: {
      'label + &': {
        marginTop: theme.spacing.unit * 3,
      },
    },
    input: {
      borderRadius: 4,
      position: 'relative',
      backgroundColor: theme.palette.background.paper,
      border: '1px solid #ced4da',
      fontSize: 14,
      width: 'auto',
      minWidth : '120px',
      //padding: '10px 26px 10px 12px',
      padding: '10px 18px',
      transition: theme.transitions.create(['border-color', 'box-shadow']),
      textAlign : 'center',
      // Use the system font instead of the default Roboto font.
      fontFamily: [
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
      '&:focus': {
        borderRadius: 4,
        borderColor: '#80bdff',
        boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
      },
    },
  }))(InputBase);

