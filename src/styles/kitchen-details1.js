const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
    color : '#000'
  },
  button: {
    margin: theme.spacing.unit,
    backgroundColor : "#FFF"
  },
  tableHead:{
      fontWeight : 500,
      color : '#000',
      border :'none',
      background: '#f8f8f8',
      borderRight : '1px solid #b1a6a6',
      textAlign : 'center',
      cursor : 'pointer',
      fontSize : 18
  },
  headingLabel:{
    fontWeight : 600,
    color : '#000',
    marginBottom : '1.5rem'
 },
  noBorder : {
      border: 'none',
      
      padding: '0px 20px 0px 24px'
  },
  tableHeight : {
      height : '45px'
      
  },
  tableHeight2 : {
    height : '10px'  
  },
  bottomTblDiv : {
    marginTop : '3rem'
  },
  bottomTbl_Header:{
    padding : "6px 4px",
    background : "#f8f8f8",
    boxShadow : "0px 1px 5px 0px rgba(0,0,0,0.2), 0px 2px 2px 0px rgba(0,0,0,0.14), 0px 3px 1px -2px rgba(0,0,0,0.12)",
    float : 'left',
    width : '100%',
    fontWeight: 600,
    
  },
  leftHeader : {
    width : '50%',
    color : '#000',
    padding: '0 2rem',
    float : 'left'
  },
  rightHeader : {
    width : '50%',
    color : '#000',
    padding: '0 2rem',
    float : 'left'
  },
  header_title1:{color: '#2C90F3', fontWeight: 600, display : 'inline-block'},
  header_title2: {color: '#000', fontWeight: 600, display : 'inline-block', 'marginRight' : '30px' },
  active_title:{ fontWeight: 600, display : 'inline-block', color: '#0ADB26', 'marginRight' : '70px' },
  header_padding:
  {
    padding: '4px 18px 4px 16px'
  },
  table2 : {
      minWidth : 'auto'
  },
  table2Cells : {
    padding: '4px 6px',
    fontSize: '16px',
    border: 'none',
    color : '#7d7474'
  },
  table2Header : {
    padding: '10px',
    border : 'none',
    background: '#fff',
    color: '#000'
  },
  total_cell : {
    padding: '10px',
    fontSize: '16px',
    border: 'none',
    color : '#000'
  },
  detail_cell : {
    padding: '4px 6px',
    fontSize: '16px',
    border: 'none',
    color : '#000',
    fontWeight : 600
  },
  blueLabel : {
    color : '#468BF2',
    padding: '1rem 0.7rem'
  },
  table3 : {
    maxWidth : '275px'
  },
  table4 : {
    maxWidth : '300px'
  },
  contentBox : {
      padding : '4rem',
      width : '100%',
      float : 'left',
  },
  floatLeft : {
      width : '50%',
      float : 'left',
      minWidth :"250px"
  },
  rightLeft : {
    width : '50%',
    float : 'left',
    minWidth :"250px"
  },
  buttonBox : {
      width : '100%',
      float : 'left',
      textAlign : 'center',
      padding : '2rem 0'
  },
  login : {
      background : '#FFFFFF'
  },
  contentBox2 : {
    border: '1px solid #707070',
    padding : '1rem',
    width : '100%',
    float : 'left',
  },
  statusButton : {
      borderRadius : 0,
      background : '#FFF',
      color : '#000',
      border: 'none',
      boxShadow: 'none',
      padding : '2px 16px'
  },
  dropDown : {
    borderRadius: '0',
    padding: '8px 26px'
  },
  cancelBtn : {
      background : '#fff',
      color : '#373535',
      marginRight : '10px'
  },
  saveBtn: {
      background : '#82EEAD',
      border: '1px solid #077712',

  },
  header:{padding : '1rem 2rem'}
});

export default styles;
