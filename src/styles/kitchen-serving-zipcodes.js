
export const styles = theme => ({
  root: {
    width: '100%',
    //marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
    color : '#000'
  },
  button: {
    margin: theme.spacing.unit,
    backgroundColor : "#FFF"
  },
  tableHeadCell : {
      padding : '4px 10px 4px 10px',
      fontWeight : 600,
      color : '#000',
      fontSize : 18
  },
  tableBodyCell : {
    padding : '4px 20px 4px 24px',
    color : '#000',
    padding : '4px 15px 4px 8px'
}
});
