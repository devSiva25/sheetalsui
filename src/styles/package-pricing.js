
import InputBase from '@material-ui/core/InputBase';
import { withStyles } from '@material-ui/core/styles';
import TableCell from '@material-ui/core/TableCell';

export const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 16,
  }
}))(TableCell);

export const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  button: {
    margin: theme.spacing.unit,
  },
  table: {
    maxWidth: 570,
  },
  row: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
  header : {
    fontSize:16,
    color : '#333',
    fontWeight : 600,
    height: '40px'
  },
  header_padding:
  {
    padding: '4px 18px 4px 16px',
    
  },
  total_cell : {
    padding: '4px 18px 4px 16px',
    color : '#5682FC'
  },
  leftBox :
  {
     float: "left",
     width : "50%",
     padding: "2.5rem 5px 1rem"
  },
  add_exception_label :{
    color : "#5682FC",
    cursor : "pointer"
  },
  margin  :{
    margin : '0 10px'
  },
  table_row : {
    height : 'auto'
  },
  cancelButton :{
     margin : theme.spacing.unit,
     color : '#000'
  },
  button_footer : {
     textAlign : 'center',
     padding : '10px 0',
     width : '100%',
     float : 'left'
  },
  saveButton : {
    background : '#F6DD97',
    color : '#000000'
  },
  table2 : {
    width : 'auto'
  },
  textField: {
    marginLeft: 0,//theme.spacing.unit,
    marginRight: 0, //theme.spacing.unit,
  },
  textbox : {
    padding: '5px 10px',
    height: '23px',
    width : '80px'
  }
});

export const BootstrapInput = withStyles(theme => ({
  root: {
    'label + &': {
      marginTop: theme.spacing.unit * 3,
    }
  },
  input: {
    height : 30,
    color : '#707071',
    borderRadius: 3,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid rgba(112, 112, 112, 0.2)',
    fontSize: 16,
    //padding: '10px 26px 10px 12px',
    padding: '5px 20px',
    width : '155px',
    //padding: '5px 18px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:focus': {
      borderRadius: 4,
      borderColor: '#80bdff',
      boxShadow: '0px 3px 6px rgba(0, 0, 0, 0.16)',
    },
  },
}))(InputBase);

export default styles;
