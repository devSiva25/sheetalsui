import InputBase from '@material-ui/core/InputBase';
import { withStyles } from '@material-ui/core/styles';
export const styles = theme => ({
headRowHeight:{
    height : 35
},  
bodyRow :{
    height : '35px',padding : '5px 0'
},
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
    color : '#000'
  },
  button: {
    //margin: theme.spacing.unit,
    backgroundColor : "#FFF",
    padding : '8px 16px',
    border : '1px solid #e2e2e2',
    boxShadow : 'none'
  },
  cellPadding : {
    padding : '4px 20px 4px 20px',
    border: 'none',
    color : '#2D2B2B',
    fontWeight : 600,
    fontSize : 16
  },
  headCellPadding : {
    padding : '4px 20px 4px 20px',
    color : '#2D2B2B',
    fontWeight : 600,
    backgroundColor : '#F8F8F8',
    fontSize : 16
  }
});

export 
const BootstrapInput = withStyles(theme => ({
  root: {
    'label + &': {
      marginTop: theme.spacing.unit * 3,
    }
  },
  input: {
    height : 30,
    color : '#707071',
    borderRadius: 3,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid rgba(112, 112, 112, 0.2)',
    fontSize: 16,
    //padding: '10px 26px 10px 12px',
    padding: '5px 20px',
    width : '155px',
    //padding: '5px 18px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:focus': {
      borderRadius: 4,
      borderColor: '#80bdff',
      boxShadow: '0px 3px 6px rgba(0, 0, 0, 0.16)',
    },
  },
}))(InputBase);

