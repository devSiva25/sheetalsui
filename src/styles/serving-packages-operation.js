
export const styles = theme => ({
  root: {
    width: '100%',
    //marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
    boxShadow : 'none',
    border : '1px solid #b4b4b4'
  },
  table: {
    minWidth: 700,
    color : '#000'
  },
  button: {
    margin: theme.spacing.unit,
    backgroundColor : "#FFF"
  },
  tableHeadCell : {
      padding : '4px 4px 4px 12px',
      fontWeight : 400,
      color : '#3B86FF',
      fontSize :'16px'
      
  },
  tableBodyCell : {
    padding : '4px 4px 4px 12px',
    color : '#707071',
    fontSize : '16px'
  },
  filterBox :{
    marginBottom : '22px'
  },
  heading : {
      color : '#3B86FF',
      fontSize : '25px',
      fontWeight : '400',
      verticalAlign : 'bottom'
  },
  actionButton : {
    margin: theme.spacing.unit,
    color : "#3B86FF",
    backgroundColor : "#FFF"
  },
  detailBoxWrapper: {
    marginTop : '50px',
    marginBottom: '50px',
    float: 'left',
    width: '100%'
  },
  detailBox : {
    marginRight : '182px',
    marginLeft : '498px',
    border: ' 1px solid #e0e0e0',
    width : '100%',
    maxWidth : '436px',
    height: '330px',
    padding : '20px'
  },
  detailBoxHeading : {
      fontWeight : 600,
      width : '100%',
      float : 'left',
      color : '#000'
  },
  details : {
    padding : '10px 0', 
    width : '100%', 
    float : 'left',
    fontSize: '14px'
  },
  statusButton : {
    borderRadius : 0,
    background : '#FFF',
    color : '#000',
    border: 'none',
    boxShadow: 'none',
    padding : '2px 16px'
  },
  detailsButton : {
      padding : '10px',
      textAlign : 'center',
  },
  saveButton : {
      background : '#02071E',
      color : '#7CDE60',
      border: '1px solid #02071E',
      borderRadius : '0'
  },
  cancelButton : {
    background : '#FFF',
    color : '#7CDE60',
    border: '1px solid #FFF',
    borderRadius : '0px'
  },
  btnLabel :{
    fontSize :18,
    color : '#707071',
    marginTop : '15px',
    display : 'inline-block'
  },
  pullRight : {
    float : 'right'
  }
});
