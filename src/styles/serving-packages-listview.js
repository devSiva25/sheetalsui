import TableCell from '@material-ui/core/TableCell';
import { withStyles } from '@material-ui/core/styles';

export const CustomTableCell = withStyles(theme => ({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    }
  }))(TableCell);


export const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
    color : '#161716'
  },
  button: {
    backgroundColor : "#FFF"
  },
  tableHead:{
      fontWeight : 600,
      color : '#000',
      border :'none',
      background: '#f8f8f8',
      fontSize: 16
  },
  noBorder : {
      border: 'none',
      fontSize: 16,
      padding: '0px 20px 0px 24px'
  },
  tableHeight : {
      height : '30px'
      
  },
  bottomTblDiv : {
    marginTop : '3rem'
  },
  bottomTbl_Header:{
    padding : "6px 4px",
    background : "#f8f8f8",
    boxShadow : "0px 1px 5px 0px rgba(0,0,0,0.2), 0px 2px 2px 0px rgba(0,0,0,0.14), 0px 3px 1px -2px rgba(0,0,0,0.12)",
    float : 'left',
    width : '100%',
    fontWeight: 600,
    
  },
  leftHeader : {
    width : '50%',
    color : '#242222',
    padding: '0 2rem',
    float : 'left'
  },
  rightHeader : {
    width : '50%',
    color : '#242222',
    padding: '0 2rem',
    float : 'left'
  },
  header : {
    fontSize:16,
    color : '#333',
    fontWeight : 500,
    height: 'auto',
  },
  header_padding:
  {
    padding: '4px 18px 4px 16px'
  },
  table2 : {
      minWidth : 'auto'
  },
  table2Cells : {
    padding: '10px',
    border: 'none',
    color : '#7d7474',
    fontSize: 16
  },
  table2Header : {
    padding: '10px 5px',
    border : 'none',
    background: '#fff',
    color: '#323332',
    fontSize: 16,
    fontWeight : 600
  },
  total_cell : {
    padding: '10px 5px',
    fontSize: '16px',
    border: 'none',
    color : '#323332'
  },
  person_cell : {
    padding: '10px',
    fontSize: '16px',
    border: 'none',
    color : '#161716',
    fontWeight : 600
  },
  blueLabel : {
    color : '#468BF2',
    padding: '1rem 0.7rem'
  },
  table3 : {
    maxWidth : '230px'
  },
  table4 : {
    maxWidth : '300px'
  },
  heading : {
    color : '#4BDF4B',
    fontWeight : '600',
    fontSize : '20px',
    marginRight : '15px',
    display: "inline-block",
    padding: "10px 0"
  },
  filterBox : {
    margin : '20px 0'
  }

});