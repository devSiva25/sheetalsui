import { fade } from '@material-ui/core/styles/colorManipulator';

const drawerWidth = 307;

const styles = theme => ({
  root: {
    display: 'flex',
  },
  drawer: {
    [theme.breakpoints.up('md')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    marginLeft: drawerWidth,
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
    },
    boxShadow: 'none',
    height : '75px'
  },
  bgnBorder :{
    backgroundColor : "#f9f9f9 !important",
    //backgroundColor: '#f5f5f5 !important',
    borderBottom: '1px solid rgba(112, 112, 112, 0.5)',
    borderLeft: 'none'
  },
  menuButton: {
    marginRight: 20,
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3 + ' 0',
    width: '90%'
  },
  logoBox : {
    padding: '7px 4rem',
    color : '#707070'
  },
  logoText : {
      fontSize : "25px",
      fontWeight : 600,
      fontFamily : "'Roboto', sans-serif",
      color : '#707070'
  },
  textField : {
    flex: '1',
    marginLeft: '8px',
    background : '#FFF',
    paddingLeft: '12px',
    width : '602px',
    border: '1px solid #b4b4b4',
    height : 40
  },
  button : {
      background : 'rgba(59, 134, 255, 0.8)',
      marginLeft: '10px',
      //height: '32px',
      padding : "11px 35px 8px",		
      marginTop:"-6px"	
  },
  pullRight : {
      float : "right"
  },
  userName : {
      color : "#000",
      fontSize:"21px",		
      fontWeight:"600"	
  },
  location : {
      color : "#F5B22B",
      fontWeight : 600
  },
  user_dd : {
    color : "#F5B22B",
    fontWeight : 600,
    padding: 2
},
  menu_button : {
    paddingTop : "5px",
    paddingBottom : "5px"
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing.unit * 2,
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing.unit * 3,
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color : '#b4b4b4'
  },
  inputRoot: {
    color: 'inherit',
    width: '100%',
  },
  inputInput: {
    border : '1px solid #b4b4b4',
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 10,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '80%',
      maxWidth : 600
    },
    color : '#000'
  },
  listMenu : {
    padding : '4.3rem 2rem'
  },
  menuText : {
    fontWeight : 600,
    color : '#707070',
    fontFamily : "'Roboto', sans-serif",
    fontSize : '20px',
    lineHeight : "18px"
  },
  menuTextActive : {
    fontWeight : 600,
    color : '#707070',
    fontFamily : "'Roboto', sans-serif",
    fontSize : '20px',
    lineHeight : "18px",
    color : "#F5B22B"
  },
  divider : {
    background : 'none',
    height: '2rem'
  }
});

export default styles;