
import InputBase from '@material-ui/core/InputBase';
import { withStyles } from '@material-ui/core/styles';

export const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
    color : '#000',
    border : '1px solid #e0e0e0'
  },
  button: {
    margin: theme.spacing.unit,
    backgroundColor : "#FFF"
  },
  tableHead:{
      fontWeight : 500,
      color : '#000',
      border :'none',
      background: '#f8f8f8',
      borderRight : '1px solid #b1a6a6',
      textAlign : 'center',
      fontSize : 16,
      height : '45px'
  },
  headingLabel:{
    fontWeight : 600,
    color : '#000',
    marginBottom : '1.5rem'
 },
  noBorder : {
      border: 'none',
      fontSize :16,
      padding: '0px 20px 0px 24px'
  },
  tableHeight2 : {
    height : '10px'  
  },
  bottomTblDiv : {
    marginTop : '3rem'
  },
  bottomTbl_Header:{
    padding : "6px 4px",
    background : "#f8f8f8",
    boxShadow : "0px 1px 5px 0px rgba(0,0,0,0.2), 0px 2px 2px 0px rgba(0,0,0,0.14), 0px 3px 1px -2px rgba(0,0,0,0.12)",
    float : 'left',
    width : '100%',
    fontWeight: 600,
    
  },
  leftHeader : {
    width : '50%',
    color : '#000',
    padding: '0 2rem',
    float : 'left'
  },
  rightHeader : {
    width : '50%',
    color : '#000',
    padding: '0 2rem',
    float : 'left'
  },
  header : {
    fontSize:12,
    color : '#333',
    fontWeight : 500,
    height: 'auto',
  },
  header_padding:
  {
    padding: '4px 18px 4px 16px'
  },
  table2 : {
      minWidth : 'auto',
      border : '1px solid #e0e0e0'
  },
  table2Cells : {
    color : '#000',
    fontWeight : '600'
  },
  table2Header : {
    padding: '10px',
    border : 'none',
    background: '#fff',
    color: '#000'
  },
  total_cell : {
    padding: '10px',
    fontSize: '12px',
    border: 'none',
    color : '#000'
  },
  detail_cell : {
    padding: '10px 6px',
    fontSize: '12px',
    border: 'none',
    color : '#000',
    fontWeight : 500
  },
  blueLabel : {
    color : '#468BF2',
    padding: '1rem 0.7rem'
  },
  table3 : {
    maxWidth : '200px'
  },
  table4 : {
    maxWidth : '300px'
  },
  contentBox : {
      padding : '0rem',
      width : '100%',
      float : 'left',
  },
  floatLeft : {
      width : '60%',
      float : 'left',
      minWidth :"350px",
      margin : '1rem 0'
  },
  floatRight : {
    width : '30%',
    float : 'right',
    minWidth :"250px",
    margin : '1rem 0',
    padding : '1rem 1.5rem',
    border: '1px solid #e0e0e0',
    background : '#f5f5f5'
  },
  rightLeft : {
    width : '50%',
    float : 'left',
    minWidth :"250px"
  },
  buttonBox : {
      width : '100%',
      float : 'left',
      textAlign : 'center',
      padding : '2rem 0'
  },
  login : {
      background : '#FFFFFF'
  },
  contentBox2 : {
    border: '1px solid #707070',
    
    width : '100%',
    float : 'left',
  },
  statusButton : {
      borderRadius : 0,
      background : '#FFF',
      color : '#000',
      border: 'none',
      boxShadow: 'none',
      padding : '2px 16px'
  },
  dropDown : {
    borderRadius: '0',
    padding: '8px 26px'
  },
  cancelBtn : {
      background : 'none',
      color : '#312E2E',
      float : 'right',
      boxShadow : 'none'
  },
  saveBtn: {
      background : '#82EEAD',
      border: '1px solid #077712',
  },
  textField : {
    marginRight: theme.spacing.unit,
    width : '100%'
  },
  inputText : {
      height : '35px',
      width : '100%'
  },
  leftContent : {
      float : 'left',
      color : '#071D2E',
      fontWeight : 600,
      maxWidth: '290px',
      width: '30%',
      paddingRight: '2rem'
  },
  contentTable :{
      float: 'left',
      width : '100%',
      marginTop : '1.5rem'
  },
  cellDays : {
      color : '#000',
      fontWeight : '600'
  },
  cellBorder : {
      border: '1px solid #e0e0e0'
  },
  tableBodyRow : {
      height : '35px'
  },
  details : {
      float : 'left',
      width : '100%',
      margin: '0.5rem 0'
  },
  saveButton : {
      background : '#3B86FF',
      color : '#FFF',
      width : '100%'
  },
  headCell : {
      fontSize :16
  }

});


export const BootstrapInput = withStyles(theme => ({
    root: {
      'label + &': {
        marginTop: theme.spacing.unit * 3,
      },
    },
    input: {
      borderRadius: 4,
      position: 'relative',
      backgroundColor: theme.palette.background.paper,
      border: '1px solid #ced4da',
      fontSize: 14,
      width: 'auto',
      //padding: '10px 26px 10px 12px',
      padding: '10px 18px',
      transition: theme.transitions.create(['border-color', 'box-shadow']),
      // Use the system font instead of the default Roboto font.
      fontFamily: [
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
      '&:focus': {
        borderRadius: 4,
        borderColor: '#80bdff',
        boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
      },
    },
  }))(InputBase);
  