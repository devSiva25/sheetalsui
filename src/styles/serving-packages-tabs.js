
const styles = theme => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    width: '100%',
    padding : '2rem'
  },
  button: {
    marginRight: theme.spacing.unit,
  },
  instructions: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
  },
  filterBox :{
    marginBottom : '2rem'
  },
  button : {
    background : '#FFF'
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
  rightIcon : {
    float : 'right'
  },
  cancelButton :{
    margin : theme.spacing.unit,
    color : '#000 !important',
    border : '1px solid #7A77E8'
 },
 button_footer : {
    textAlign : 'center',
    padding : '10px 0',
    width : '100%',
    float : 'left'
 },
 saveButton : {
   background : '#F6DD97',
   color : '#000000',
   border :  '1px solid #000000'
 },
});

export default styles;