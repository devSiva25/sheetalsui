
import TableCell from '@material-ui/core/TableCell';
import { withStyles } from '@material-ui/core/styles';
import purple from '@material-ui/core/colors/purple';

export const CustomTableCell = withStyles(theme => ({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    }
  }))(TableCell);
  
export const styles = theme => ({
    root: {
      width: '100%',
      marginTop: theme.spacing.unit * 3,
      overflowX: 'auto',
    },
    button: {
      margin: theme.spacing.unit,
    },
    table: {
      maxWidth: 500,
    },
    row: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.background.default,
      },
    },
    header : {
      fontSize:12,
      color : '#333',
      fontWeight : 600,
      height: 'auto',
      padding : '10px 0'
    },
    header_padding:
    {
      padding: '4px 18px 4px 16px',
      border : 'none',
      background : '#FFF',
      color : '#000'
    },
    total_cell : {
      padding: '4px 18px 4px 16px',
      color : '#5682FC'
    },
    leftBox :
    {
       float: "left",
       width : "50%",
       padding: "2.5rem 5px 1rem"
    },
    add_exception_label :{
      color : "#5682FC"
    },
    margin  :{
      margin : '0 10px'
    },
    table_row : {
      height : 'auto'
    },
    cancelButton :{
       margin : theme.spacing.unit,
       color : '#000'
    },
    button_footer : {
       textAlign : 'center',
       padding : '10px 0',
       width : '100%',
       float : 'left'
    },
    saveButton : {
      background : '#F6DD97',
      color : '#000000'
    },
    dropDownPadding : {
        padding: '5px 18px'
    },
    switchButton : {
        minWidth : '50px',
        height : '25px',
        padding: '1px',
        margin : '0',
        background: '#fff',
        border: '1px solid #000',
        borderRadius: '0'
    },
    colorSwitchBase: {
      color: purple[300],
      '&$colorChecked': {
        color: purple[500],
        '& + $colorBar': {
          backgroundColor: purple[500],
        },
      },
    },
    colorBar: {},
    colorChecked: {},
    iOSSwitchBase: {
      '&$iOSChecked': {
        color: theme.palette.common.white,
        '& + $iOSBar': {
          backgroundColor: '#52d869',
        },
      },
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
        easing: theme.transitions.easing.sharp,
      }),
    },
    iOSChecked: {
      transform: 'translateX(15px)',
      '& + $iOSBar': {
        opacity: 1,
        border: 'none',
      },
    },
    iOSBar: {
      borderRadius: 13,
      width: 42,
      height: 26,
      marginTop: -13,
      marginLeft: -21,
      border: 'solid 1px',
      borderColor: theme.palette.grey[400],
      backgroundColor: theme.palette.grey[50],
      opacity: 1,
      transition: theme.transitions.create(['background-color', 'border']),
    },
    iOSIcon: {
      width: 24,
      height: 24,
    },
    iOSIconChecked: {
      boxShadow: theme.shadows[1],
    }  
  });
