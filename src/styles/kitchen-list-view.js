import InputBase from '@material-ui/core/InputBase';
import { withStyles } from '@material-ui/core/styles';



export const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
    
  },
  title : {color: '#2C90F3', fontWeight: 600, display : 'inline-block', 'marginRight' : '30px' , fontSize: 20, verticalAlign: 'bottom' },
  table: {
    minWidth: 700,
    color : '#000'
  },
  button: {
    margin: theme.spacing.unit,
    backgroundColor : "#FFF"
  },
  tableHead:{
      fontWeight : 600,
      color : '#000',
      border :'none',
      background: '#f8f8f8',
      padding: '4px 10px 4px 14px',
      fontSize: 16
  },
  noBorder : {
      border: 'none',
      padding: '4px 10px 4px 14px',
      fontSize: 16
      //padding: '0px 20px 0px 24px'
  },
  tableHeight : {
      height : '35px'
      
  },
  bottomTblDiv : {
    marginTop : '3rem'
  },
  bottomTbl_Header:{
    padding : "6px 4px",
    background : "#f8f8f8",
    boxShadow : "0px 1px 5px 0px rgba(0,0,0,0.2), 0px 2px 2px 0px rgba(0,0,0,0.14), 0px 3px 1px -2px rgba(0,0,0,0.12)",
    float : 'left',
    width : '100%',
    fontWeight: 600,
    
  },
  leftHeader : {
    width : '50%',
    color : '#000',
    padding: '0 2rem',
    float : 'left'
  },
  rightHeader : {
    width : '50%',
    color : '#000',
    padding: '0 2rem',
    float : 'left'
  },
  header : {
    fontSize:12,
    color : '#333',
    fontWeight : 500,
    height: 'auto',
  },
  header_padding:
  {
    padding: '4px 18px 4px 16px'
  },
  table2 : {
      minWidth : 'auto'
  },
  table2Cells : {
    padding: '10px',
    fontSize: '12px',
    border: 'none',
    color : '#7d7474'
  },
  table2Header : {
    padding: '10px',
    border : 'none',
    background: '#fff',
    color: '#000'
  },
  total_cell : {
    padding: '10px',
    fontSize: '12px',
    border: 'none',
    color : '#000'
  },
  person_cell : {
    padding: '10px',
    fontSize: '12px',
    border: 'none',
    color : '#000',
    fontWeight : 500
  },
  blueLabel : {
    color : '#468BF2',
    padding: '1rem 0.7rem'
  },
  table3 : {
    maxWidth : '200px'
  },
  table4 : {
    maxWidth : '300px'
  },

});


export const BootstrapInput = withStyles(theme => ({
  root: {
    'label + &': {
      marginTop: theme.spacing.unit * 3,
    }
  },
  input: {
    height : 30,
    color : '#707071',
    borderRadius: 3,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid rgba(112, 112, 112, 0.2)',
    fontSize: 16,
    //padding: '10px 26px 10px 12px',
    padding: '5px 20px',
    width : '155px',
    //padding: '5px 18px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:focus': {
      borderRadius: 4,
      borderColor: '#80bdff',
      boxShadow: '0px 3px 6px rgba(0, 0, 0, 0.16)',
    },
  },
}))(InputBase);